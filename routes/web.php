<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// user
$router->get('livetracking', 'VisitorsController@liveTracking');
$router->post('user/login','UserController@authenticateUser');
$router->post('user/forgotpassword','UserController@forgotpassword');
$router->post('user/updatepassword','UserController@updateResetPassword');
$router->post('user/create','UserController@createUser');
$router->Get('user/roletypemaster','UserController@roletypemaster');
$router->post('user/rolecreate','UserController@rolecreate');
$router->get('generateuserdomainpopup', 'RenderController@generateUserDomainPopup');
$router->post('contactcreate','ContactsController@contactCreate');
$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
	// user
    
    
    $router->get('validatetoken','UserController@validateToken');
    $router->post('user/changepassword','UserController@changePassword');
    $router->get('user/fetchuserdetail','UserController@fetchUserDetail');
    $router->post('user/updateuserdetail','UserController@updateUserDetail');
    $router->post('user/deletestoreuser','UserController@deleteUser');
    $router->post('getuserbyid','UserController@getuserbyid');
	// campaign 
    //$router->post('createpopupcampaign','PopupController@createpopupcampaign');
    //$router->post('getpopupmaster','PopupController@getpopupmaster');    
    $router->post('getpopuptemplatebyid','PopupController@getpopuptemplatebyid');
    //$router->post('popupsavejson','PopupController@popupsavejson');    
    //$router->post('getcampaigncontactlist', 'ContactsController@getCampaignContactList');
    //$router->post('getusercontactlist', 'ContactsController@getUserContactList');

    // image
    $router->post('addimage','imagelibraryController@addimage');
    $router->post('editimage','imagelibraryController@editimage');
    $router->post('listimage','imagelibraryController@listimage');
    $router->post('deleteimage','imagelibraryController@deleteimage');

    $router->post('subuserscreate','UserController@subuserscreate');
    //$router->post('channelcreate','UserController@channelcreate');
    //$router->post('channellist','UserController@channellist');
    $router->post('getallsubusers','UserController@getallsubusers');
    //$router->post('assignchannel','UserController@assignchannel');

    $router->post('adddomain', 'DomainController@addDomain');
    $router->post('updatedomain', 'DomainController@updateDomain');
    $router->post('deletedomain', 'DomainController@deleteDomain');
    $router->post('getuserdomainlist', 'DomainController@getUserDomainList');

    $router->post('createchannel', 'ChannelsController@createChannel');
    $router->post('addusertochannel', 'ChannelsController@addUserToChannel');
    $router->post('getuserchannellist', 'ChannelsController@getUserChannelList');
    $router->post('createpopup', 'PopupController@createPopup');
    $router->post('getpopuptemplatebytype', 'PopupController@getPopupTemplateByType');
    $router->post('savepopupjson', 'PopupController@savePopupJson');
    $router->post('savepopuptrigger','PopupController@savePopupTrigger');
    $router->post('getpopuptrigger', 'PopupController@getPopupTrigger');
    $router->post('getpopuplist', 'PopupController@getPopupList');
    $router->post('getpopuparchivelist', 'PopupController@getPopupArchiveList');
    $router->post('togglearchivestatus', 'PopupController@toggleArchiveStatus');
    $router->post('togglepopupstatus', 'PopupController@togglePopupStatus');
    $router->post('getpopupcontactlist', 'ContactsController@getPopupContactList');
    $router->post('getchannelcontactlist', 'ContactsController@getChannelContactList');
});
$router->post('testsendmail','MailConfigcontroller@Sendgridmail');




