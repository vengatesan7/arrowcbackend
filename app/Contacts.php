<?php
namespace App; 
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;
class Contacts extends Model{
	protected $primaryKey	= 'contact_id';
	protected $fillable		= ['popup_id', 'ip_address'];   
}
?>