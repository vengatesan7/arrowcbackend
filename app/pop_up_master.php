<?php namespace App; 
 use HasApiTokens, Notifiable;
    use Illuminate\Database\Eloquent\Model;
    

    class pop_up_master extends Model
	{
    	protected $primaryKey = 'pop_up_id';
        protected $fillable = ['pop_up_name','user_id', 'pop_up_img_prvw_url','pop_up_json_code','archive_status','campaign_type_id','pop_up_type'];   
    }
?>