<?php
namespace App; 
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;
class VisitorLogs extends Model{
	protected $primaryKey	= 'visitor_log_id';
	protected $fillable		= ['visitor_id','url','popup_id','domain'];   
}
?>
