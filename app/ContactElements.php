<?php
namespace App; 
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;
class ContactElements extends Model{
	protected $primaryKey	= 'element_id';
	protected $fillable		= ['contact_id', 'element_name', 'element_value'];   
}
?>