<?php
	namespace App;
	use HasApiTokens, Notifiable;
	use Illuminate\Database\Eloquent\Model;
	class Channels extends Model{
		protected $primaryKey = 'channel_id';
		protected $fillable = ['name', 'created_user_id'];
	}
?>
