<?php namespace App;
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;


class user extends Model
{
    protected $primaryKey = 'user_id';
    protected $fillable = ['email', 'password', 'first_name','last_name','mobile_number','user_token','role_id','created_user_id','ip_address'];
}
?>
