<?php
namespace App\Http\Controllers;
use App\Contacts;
use App\ContactElements;
use App\Popups;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use DB;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Log;
 
class ContactsController extends Controller {         
    public function contactcreate(Request $request){
        $this->validate($request, [
            'popup_id' => 'integer|required',
        ]);  
        try{            
            $popup_id   = $_POST['popup_id'];    
            $data       = ['popup_id'=>$popup_id,'ip_address'=>$_SERVER['REMOTE_ADDR']];
            $contact    = Contacts::create($data);
            $name       = "";
            foreach ($_POST as $key => $value) {
                if(is_array($value)){
                    $value          = $value;
                    $element_value  = json_encode($value,true);
                }else{
                    $element_value = $value;
                }
                $value  = str_replace('"','',$value);                
                if($key =="popup_id" || $key=='submit' || $key == 'actionUrl'){
                }else{
                    $element_name   = $key;
                    /*if($element_name =="name" || $element_name =="Name" || $element_name =="NAME"){
                        $name   = $element_value;
                    }
                    if($element_name =="email" || $element_name =="Email" || $element_name =="EMAIL"){
                        $subject ="Submiting form";
                    }*/
                    $element    = ['contact_id'=> $contact->contact_id,'element_name'=>$element_name,'element_value'=>$element_value];
                    $getelement = ContactElements::create($element);
                }
            }                                    
            return response()->json(['status' => 'success','message' => 'Contact Saved Successfully'],200);
        }catch(Exception $e){
            Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
    public function getPopupContactList( Request $request ){
        $user_id = $request->auth->user_id; 
        $this->validate($request, [
            'popup_id' => 'integer|required',
        ]);
        try{            
            $contacts               = Contacts::select('contact_id','created_at','ip_address')->where('popup_id',$request->popup_id)->get();
            $count                  = count($contacts);
            $contactarray           = $contacts->toArray();
            $name                   = $email = '';
            $check                  = $test = $final = [];
            foreach ($contactarray as $key => $contact) {
                $element    = ContactElements::select('element_id','element_name','element_value')->where('contact_id','=',$contact['contact_id'])->get();
                $check      = [];
                foreach($element as $rows){
                    if( ($rows->element_name != 'contact_id') ){
                        $check[$rows->element_name]=$rows->element_name;
                        $check[$rows->element_name] = $rows->element_value;
                    }
                    $elementName = strtolower($rows->element_name);
                    if(($elementName == 'name') || ($elementName == 'first name') || ($elementName == 'full name') || ($elementName == 'full_name') || ($elementName == 'first_name')){
                        $name = $rows->element_value;
                    }
                    if(($elementName == 'email') || ($elementName == 'mail') || ($elementName == 'e_mail') || ($elementName == 'emailaddress') || ($elementName == 'email_address')){
                        $email = $rows->element_value;                            
                    }
                }                    
                $test['contact_id']             = $contact['contact_id'];
                $test['created_at']             = $contact['created_at'];
                $test['ip_address']             = $contact['ip_address'];                    
                $test['name']                   = $name;
                $test['email']                  = $email;
                $test['whole_data']             = $check;
                $final[$contact['contact_id']]  = $test;
            }
            $contactarray   =  $final;
            return response()->json(['status' => 'success','message' => 'Contacts Listed By Popup Successfully','contacts'=>$contactarray,'count'=>$count],200);
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }

    public function getChannelContactList( Request $request ){
        $user_id = $request->auth->user_id;
        $this->validate($request, [
            'channel_id' => 'integer|required',
        ]);
        try{
            $popupIds       = Popups::where('channel_id', $request->channel_id)->pluck('popup_id')->toArray();
            $contacts       = array();
            $contactarray   = array();
            $count          = 0;
            if( is_array($popupIds) && count($popupIds) > 0 ){
                $contacts               = Contacts::select('contact_id','created_at','ip_address')->whereIn('popup_id',$popupIds)->get();
                $count                  = count($contacts);
                $contactarray           = $contacts->toArray();
                $name                   = $email = '';
                $check                  = $test = $final = [];
                foreach ($contactarray as $key => $contact) {
                    $element    = ContactElements::select('element_id','element_name','element_value')->where('contact_id','=',$contact['contact_id'])->get();
                    $check      = [];
                    foreach($element as $rows){
                        if( ($rows->element_name != 'contact_id') ){
                            $check[$rows->element_name]=$rows->element_name;
                            $check[$rows->element_name] = $rows->element_value;
                        }
                        $elementName = strtolower($rows->element_name);
                        if(($elementName == 'name') || ($elementName == 'first name') || ($elementName == 'full name') || ($elementName == 'full_name') || ($elementName == 'first_name')){
                            $name = $rows->element_value;
                        }
                        if(($elementName == 'email') || ($elementName == 'mail') || ($elementName == 'e_mail') || ($elementName == 'emailaddress') || ($elementName == 'email_address')){
                            $email = $rows->element_value;                            
                        }
                    }                    
                    $test['contact_id']             = $contact['contact_id'];
                    $test['created_at']             = $contact['created_at'];
                    $test['ip_address']             = $contact['ip_address'];                    
                    $test['name']                   = $name;
                    $test['email']                  = $email;
                    $test['whole_data']             = $check;
                    $final[$contact['contact_id']]  = $test;
                }
                $contactarray   =  $final;            
            }
            return response()->json(['status' => 'success','message' => 'contacts Listed By Channel successfully','contacts'=>$contactarray,'count'=>$count],200);
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
}
