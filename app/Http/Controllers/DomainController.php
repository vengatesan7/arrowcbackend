<?php
namespace App\Http\Controllers;
use App\Domains;
use App\PopupDomainRelations;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Http\Response;
use InnoCraft\Experiments\Experiment;
use Validator;
use Log;
use DB;
class DomainController extends Controller{
	public function addDomain( Request $request ){
		$user_id = $request->auth->user_id;		
		$this->validate($request, [
			'domain_name' => 'required',			
		]);
		try{			
			$domain = Domains::where('user_id', $user_id)->where('domain_name', $request->domain_name)->first();
			if( $domain == Null ){
				$data			= ['user_id' => $user_id, 'domain_name' => $request->domain_name];
				$domainId	= Domains::create($data);				
			}
			return response()->json(['status' =>'success', 'message'=> 'Domain added successfully', 'data' => $domainId],200);
		}catch(Exception $e){
			Log::info($e); 
			return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
		}
	}
	public function updateDomain( Request $request ){
		$user_id = $request->auth->user_id;  
		$this->validate($request, [
			'domain_id'		=> 'required|exists:domains',
			'domain_name'	=> 'required',			
		]);
		try{
			$domain = Domains::where('user_id', $user_id)->where('domain_id', $request->domain_id)->first();
			if( $domain != Null ){
				$domain->domain_name	= $request->domain_name;
				$domain->save();
			}
			return response()->json(['status' =>'success', 'message'=> 'Domain updated successfully'],200);
		}catch(Exception $e){
			Log::info($e); 
			return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
		}
	}
	public function deleteDomain( Request $request ){
		$user_id = $request->auth->user_id;
		$this->validate($request, [
			'domain_id'	=> 'required|exists:domains',			
		]);
		try{
			$domain = Domains::where('user_id', $user_id)->where('domain_id', $request->domain_id)->first();
			if( $domain != Null ){				
				$domain->delete();
			}
			return response()->json(['status' =>'success', 'message'=> 'Domain deleted successfully'],200);
		}catch(Exception $e){
			Log::info($e);
			return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
		}
	}
	public function getUserDomainList ( Request $request ){
		$user_id = $request->auth->user_id;
		try{
			$userDomains	= Domains::select( 'domains_id', 'domain_name', 'domain_status' )->where('user_id', $user_id)->get()->toArray();
			return response()->json(['status' =>'success', 'message'=> 'Domain lists reterived successfully', 'data' => $userDomains ],200);
		}catch(Exception $e){
			Log::info($e);
			return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
		}
	}
}
?>