<?php

namespace App\Http\Controllers;
use App\usertemplate;
use App\imagelibrary;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Validator;
use Log;
use DB;

use Laravel\Lumen\Routing\Controller as BaseController;

class imagelibraryController extends Controller
{
	 /**
     * @OA\Post(
     *     path="/addimage",
     *     operationId="/addimage",
     *     tags={"Add Image"},
     *      security={{"bearerAuth":{}}}, 
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"image"},
     *         @OA\Property(
     *           property="image",
     *           description="Image",
     *           type="string",
     *         ),
     * 			required={"image_text"},
     *         @OA\Property(
     *           property="image_text",
     *           description="Image Text",
     *           type="string",
     *         ),
     *       ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="Image Saved Successfully",  
     *         ),
     * )
     */

	public function addimage(Request $request)
    {   
     
        
        $this->validate($request, [
            'image' => 'required',
            'image_text' => 'required'
        ]);
        try {  
            $user_id            = $request->auth->user_id;
            $data 				= explode(',', $request->image)[1];
            $fileName			= $randomv=rand().'.png';
            $path               = base_path('public'). '/uploads/imagelibraries/user_'.$user_id;
            if(!file_exists($path))
            {
                mkdir($path, 0777, true);
            }
            $uploadPath		    = base_path('public'). '/uploads/imagelibraries/user_'.$user_id.'/'. $fileName;
            $data 				= base64_decode($data);
            file_put_contents($uploadPath , $data);
            
            $image_text 		= $request->image_text;
            $image_url 			='/uploads/imagelibraries/user_'.$user_id.'/'. $fileName;

            $data = ['user_id'=> $user_id,'image_alt_text'=>$image_text,'image_url'=>$image_url];
            $imagelibrary   = imagelibrary::create($data);
            return response()->json(['status' =>'success', 'message'=> 'Image Saved Successfully','imagedata'=>$data],200); 
        }
        catch(Exception $e)
        {
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
    /**
     * @OA\Post(
     *     path="/editimage",
     *     operationId="/editimage",
     *     tags={"Update Image"},
     *      security={{"bearerAuth":{}}}, 
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         required={"image_id"},
     *         @OA\Property(
     *           property="image_id",
     *           description="image Id",
     *           type="integer",
     *         ),
     *         @OA\Property(
     *           property="image",
     *           description="Update Image",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="image_text",
     *           description="Image Text",
     *           type="string",
     *         ),
     *       ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="Image Update Successfully",  
     *         ),
     * )
     */

	public function editimage(Request $request)
    {   
     
        
        $this->validate($request, [
            'image_id' => 'required|exists:imagelibraries'
        ]);
        if(isset($request->image)){
            $this->validate($request, [
            'image' => 'required'
        ]);

        }
         if(isset($request->image_text)){
            $this->validate($request, [
            'image_text' => 'required'
        ]);
            
        }
        try {  
             $user_id            = $request->auth->user_id;
            $data 				= explode(',', $request->image)[1];
           $fileName            = $randomv=rand().'.png';
            
            $uploadPath         = base_path('public'). '/uploads/imagelibraries/user_'.$user_id.'/'. $fileName;
            $data               = base64_decode($data);
            file_put_contents($uploadPath , $data);
            
            $image_text         = $request->image_text;
            $image_url          ='/uploads/imagelibraries/user_'.$user_id.'/'. $fileName;
            $data = ['user_id'=> $user_id,'image_alt_text'=>$image_text,'image_url'=>$image_url];

            $imagelibrary       = imagelibrary::find($request->image_id);
            if(isset($request->image)){
                $imagelibrary->image_url    = $image_url;
             }
             if(isset($request->image_text)){
                  $imagelibrary->image_alt_text   = $image_text;
             }
            $imagelibrary->save();
            return response()->json(['status' =>'success', 'message'=> 'Image Update Successfully','imagedata'=>$data],200); 
        }
        catch(Exception $e)
        {
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
           /**
     * @OA\Post(
     *     path="/listimage",
     *     operationId="/listimage",
     *     tags={" User Image List"},
     *      security={{"bearerAuth":{}}}, 
     *     @OA\Response(
     *         response="200",
     *         description="User Image List",  
     *  @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         
     *         ),
     *          ),
     *         ),
     *         ),
     * )
     */

    public function listimage(Request $request)
    {  

     
        $user_id=$request->auth->user_id;
        try {  
 
            $imagelibrary   = imagelibrary::select('image_id','image_url','image_alt_text')->where('user_id',$user_id)->orderBy('created_at', 'desc')->get();


            return response()->json(['status' =>'success', 'message'=> 'User Imagelibrary List','imagedata'=>$imagelibrary],200); 
        }
        catch(Exception $e)
        {
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
     /**
     * @OA\Post(
     *     path="/deleteimage",
     *     operationId="/deleteimage",
     *     tags={"Delete Image Library"},
     *      security={{"bearerAuth":{}}}, 
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"image_id"},
     *         @OA\Property(
     *           property="image_id",
     *           description="Image Id",
     *           type="integer",
     *         ),
     *       ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="Image Deleted Successfully",  
     *         ),
     * )
     */

    public function deleteimage(Request $request)
    {   
        $this->validate($request, [
            'image_id' => 'required|exists:imagelibraries'
            ]);
        try {  
            $imagelibrarie   = imagelibrary::find($request->image_id);
            $imagelibrarie->delete();
            return response()->json(['status' =>'success', 'message'=> 'Image Deleted Successfully'],200); 
        }
        catch(Exception $e)
        {
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
}
