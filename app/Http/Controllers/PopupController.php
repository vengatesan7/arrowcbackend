<?php
namespace App\Http\Controllers;
use App\PopupTemplates;
use App\Popups;
use App\campaign;
use App\PopupDomainRelations;
// use App\visitor;
use App\VisitorLogs;
use App\Contacts;
// use App\conversion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Http\Response;
use InnoCraft\Experiments\Experiment;
use Validator;
use Log;
use DB;

class PopupController extends Controller{
    /**
    * @OA\Post(
    *     path="/getpopuptemplatebytype",
    *     operationId="/getpopuptemplatebytype",
    *     tags={"Popup Template list"},
    *      security={{"bearerAuth":{}}},
    *  @OA\RequestBody(
    *     required=true,
    *     @OA\MediaType(
    *       mediaType="application/json",
    *       @OA\Schema(
    *          required={"popup_type"},
    *        @OA\Property(
    *           property="popup_type",
    *           description="popup type",
    *           type="string"
    *         ),
    *     ),
    *   ),
    * ),
    *     @OA\Response(
    *         response="200",
    *         description="Popup Template Retrieved Successfully",  
    *         ),
    * )
    */

    public function getPopupTemplateByType( Request $request ){  
        $user_id = $request->auth->user_id;  
        $this->validate($request, [
            'popup_type'    => 'required',
        ]);
        try{
            $popupTemplate  = PopupTemplates::select('popup_template_id', 'popup_template_name', 'popup_tmpl_img_prvw_url')->where('popup_type',$request->popup_type)->get();
            return response()->json(['status' =>'success', 'message'=> 'Popup Template Retrieved successfully','popupTemplate'=>$popupTemplate],200); 
        }catch(Exception $e){
            Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }        

    /**
    * @OA\Post(
    *     path="/createpopup",
    *     operationId="/createpopup",
    *     tags={"createpopup"},
    *      security={{"bearerAuth":{}}}, 
    *  @OA\RequestBody(
    *     required=true,
    *     @OA\MediaType(
    *       mediaType="application/json",
    *       @OA\Schema(
    *          required={"popup_name"},
    *        @OA\Property(
    *           property="popup_name",
    *           description="Popup Name",
    *           type="string"
    *         ),
    *        @OA\Property(
    *           property="popup_template_id",
    *           description="Popup Template Id",
    *           type="integer"
    *         ), 
    *        @OA\Property(
    *           property="domain_id",
    *           description="Domain Id",
    *           type="string"
    *         ),
    *        @OA\Property(
    *           property="channel_id",
    *           description="Channel Id",
    *           type="string"
    *         ),
    *     ),
    *   ),
    * ),
    *     @OA\Response(
    *         response="200",
    *         description="Popup Created Successfully",  
    *         ),
    * )
    */
    public function createPopup(Request $request){  
        $user_id = $request->auth->user_id;   
        $this->validate($request, [
             'popup_name'           => 'required',
             'popup_template_id'    => 'required',
             'popup_type'           => 'required',
             'domain_id'            => 'required',
             'channel_id'           => 'required',
        ]);
        try {  
            $popupTemplate      = PopupTemplates::find($request->popup_template_id);            
            $popupDataParam     = ['popup_name' => $request->popup_name, 'user_id' => $user_id, 'channel_id' => $request->channel_id, 'popup_img_prvw_url' => $popupTemplate->popup_tmpl_img_prvw_url, 'popup_json_code' => $popupTemplate->popup_template_json_code, 'popup_type' => $request->popup_type ];
            $popup              = Popups::create($popupDataParam);            
            if( $request->domain_id ){
                $dataRelation               = ['popup_id' => $popup->popup_id, 'domain_id' => $request->domain_id ];
                $campaign_domains_relation  = PopupDomainRelations::create($dataRelation);
            }
            return response()->json(['status' =>'success', 'message'=> 'Popup Created Successfully','popup'=>$popup],200); 
        }
        catch(Exception $e)
        {
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
      /**
     * @OA\Get(
     *     path="/getcampaignlist",
     *     operationId="/getcampaignlist",
     *     tags={"Pop Up Over View Data"},
     *      security={{"bearerAuth":{}}},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *     ),
     *   ),
     * ),
     *     @OA\Response(
     *         response="200",
     *         description="Campaigns retrieved Successfully",  
     *         ),
     * )
     */

    public function getcampaignlist(Request $request){  
        $user_id = $request->auth->user_id;          
        try { 
            $getcampaignlist = DB::table('campaigns')->select('campaigns.campaign_id','campaigns.campain_name','pop_up_templates.pop_up_template_id as pop_up_id','pop_up_templates.pop_up_template_name as pop_up_name','campaigns.is_status','campaigns.is_archive')
            ->join('pop_up_templates', 'pop_up_templates.campaign_id', '=', 'campaigns.campaign_id')
            ->where('campaigns.user_id',$user_id)
            ->get();
            $Campaign       = array();
            $getcampaign    = $getcampaignlist->toArray();
            if(sizeof($getcampaign) > 0){
                $Campaign = json_decode(json_encode($getcampaign),true);
                foreach ($Campaign as $key => $value) {
                    $popup_id                           = $value['pop_up_id'];
                    $visitorcount                       = VisitorLogs::select('visitor_id')->where('popup_id',$popup_id)->distinct()->count('visitor_id');
                    $viewcount                          = VisitorLogs::select('visitor_id')->where('popup_id',$popup_id)->count();
                    $conversioncount                    = Contacts::select('contact_id')->where('popup_id', $popup_id)->count();
                    $Campaign[$key]['visitorcount']     = $visitorcount;
                    $Campaign[$key]['viewcount']        = $viewcount;
                    $Campaign[$key]['conversioncount']  = $conversioncount;                   
                }
            }
            return response()->json(['status' =>'success', 'message'=> 'Campaigns retrieved successfully','getcampaignlist'=>$Campaign],200); 
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }
   

    public function getpopuptemplatebyid(Request $request)
    {  
        $user_id = $request->auth->user_id;   
       $this->validate($request, [
             'pop_up_id' => 'required',
        ]);
        try { 

            $getpopuptemplate   = Popups::find($request->pop_up_id);
            $getpopuptemplate1['pop_up_id'] = $getpopuptemplate->pop_up_template_id;
            $getpopuptemplate1['pop_up_name'] = $getpopuptemplate->pop_up_template_name;
            //$getpopuptemplate['pop_up_img_prvw_url'] = $getpopuptemplate->pop_up_tmpl_img_prvw_url;
            $getpopuptemplate1['pop_up_json_code'] = $getpopuptemplate->pop_up_template_json_code;
            return response()->json(['status' =>'success', 'message'=> 'Popup template retrieved successfully','getpopuptemplate'=>$getpopuptemplate1],200); 
        }
        catch(Exception $e)
        {
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }  
    
    /**
    * @OA\Post(
    *     path="/savepopupjson",
    *     operationId="/savepopupjson",
    *     tags={"Save Popup Json"},
    *      security={{"bearerAuth":{}}},
    *  @OA\RequestBody(
    *     required=true,
    *     @OA\MediaType(
    *       mediaType="application/json",
    *       @OA\Schema(
    *          required={"popup_id"},
    *        @OA\Property(
    *           property="popup_id",
    *           description="Popup id",
    *           type="integer"
    *         ),
    *          @OA\Property(
    *           property="popup_json_code",
    *           description="Popup Json Code",
    *           type="string"
    *         ),
    *     ),
    *   ),
    * ),
    *     @OA\Response(
    *         response="200",
    *         description="Popup Json Saved Successfully",  
    *         ),
    * )
    */

    public function savePopupJson(Request $request){
        $user_id = $request->auth->user_id;   
        $this->validate($request, [
             'popup_id'         => 'required',
             'popup_json_code'  => 'required',
        ]);
        try {
            $popup                  = Popups::find($request->popup_id);
            $popup->popup_json_code = $request->popup_json_code;
            $popup->save();
            return response()->json(['status' =>'success', 'message'=> 'Popup Json Saved Successfully'],200); 
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
    }

    public function savePopupTrigger( Request $request ){
        $user_id = $request->auth->user_id;   
        $this->validate($request, [
             'popup_id' => 'required',
             'trigger' => 'required',
        ]);
        try{            
            $popup            = Popups::find($request->pop_up_id);
            $popup->trigger   = json_encode($request->trigger);
            $popup->save();
            return response()->json(['status' =>'success', 'message'=> 'Popup Trigger Saved Successfully'],200);
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }

    public function getPopupTrigger( Request $request ){
        $user_id = $request->auth->user_id;
        $this->validate($request, [
             'popup_id' => 'required',
        ]);
        try{
            $popup      = Popups::find($request->pop_up_id);
            $trigger    = '';
            if( $popup ){
                $trigger    = json_decode($popup->trigger);
            }
            return response()->json(['status' =>'success', 'message'=> 'Popup Trigger Reterived Successfully', 'trigger' => $trigger],200);
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }    
    }

    public function getPopupList( Request $request ){
        $user_id = $request->auth->user_id;
        $this->validate($request, [             
             'channel_id'   => 'required',
        ]);
        try{
            $popups = Popups::where('channel_id', $channel_id)->where('archive_status', false)->get();
            $popupsArray    = array();
            if( $popups ){
                $popupsArray    = $popups->toArray();
                foreach ($popupsArray as $popupKey => $popup) {
                    $visitorcount                               = VisitorLogs::select('visitor_id')->where('popup_id',$popup->popup_id)->distinct()->count('visitor_id');
                    $viewcount                                  = VisitorLogs::select('visitor_id')->where('popup_id',$popup->popup_id)->count();
                    $conversioncount                            = Contacts::select('contact_id')->where('popup_id', $popup->popup_id)->count();
                    $popupsArray[$popupKey]['visitorcount']     = $visitorcount;
                    $popupsArray[$popupKey]['viewcount']        = $viewcount;
                    $popupsArray[$popupKey]['conversioncount']  = $conversioncount;                    
                }
            }
            return response()->json(['status' =>'success', 'message'=> 'Channel popup retrieved successfully','popup_lists'=>$popupsArray],200); 
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }

    public function getPopupArchiveList( Request $request ){
        $user_id = $request->auth->user_id;
        $this->validate($request, [             
             'channel_id'   => 'required',
        ]);
        try{
            $popups = Popups::where('channel_id', $channel_id)->where('archive_status', true)->get();
            $popupsArray    = array();
            if( $popups ){
                $popupsArray    = $popups->toArray();
                foreach ($popupsArray as $popupKey => $popup) {
                    $visitorcount                               = VisitorLogs::select('visitor_id')->where('popup_id',$popup->popup_id)->distinct()->count('visitor_id');
                    $viewcount                                  = VisitorLogs::select('visitor_id')->where('popup_id',$popup->popup_id)->count();
                    $conversioncount                            = Contacts::select('contact_id')->where('popup_id', $popup->popup_id)->count();
                    $popupsArray[$popupKey]['visitorcount']     = $visitorcount;
                    $popupsArray[$popupKey]['viewcount']        = $viewcount;
                    $popupsArray[$popupKey]['conversioncount']  = $conversioncount;                    
                }
            }
            return response()->json(['status' =>'success', 'message'=> 'Channel popup retrieved successfully','popup_lists'=>$popupsArray],200); 
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }

    public function toggleArchiveStatus( Request $request ){
        $user_id = $request->auth->user_id;
        $this->validate($request, [
             'popup_id'         => 'required',
             'archive_status'   => 'required',
        ]);
        try{
            $popup                  = Popups::find($request->pop_up_id);
            $popup->archive_status  = $request->archive_status;
            $popup->save();
            return response()->json(['status' =>'success', 'message'=> 'Popup Archive Status Updated'],200);
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }

    public function togglePopupStatus( Request $request ){
        $user_id = $request->auth->user_id;
        $this->validate($request, [
             'popup_id'     => 'required',
             'popup_status' => 'required',
        ]);
        try{
            $popup                  = Popups::find($request->pop_up_id);
            $popup->popup_status    = $request->popup_status;
            $popup->save();
            return response()->json(['status' =>'success', 'message'=> 'Popup Status Updated'],200);
        }catch(Exception $e){
            Log::info($e); 
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
}
