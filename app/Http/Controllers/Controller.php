<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
          /**
      * @OA\Info(title="Leadify | Accounts API Microservices", version="0.1"),
      * @OA\SecurityScheme(
      *      securityScheme="bearerAuth",
      *      in="header",
      *      name="bearerAuth",
      *      type="http",
      *      scheme="bearer",
      *      bearerFormat="JWT",
      * ),
      * @OA\Tag(
      *     name="Auth",
      *     description="Auth endpoints",
      * )
      * @OA\Tag(
      *     name="Users",
      *     description="Users endpoints",
      * )
      */

}
