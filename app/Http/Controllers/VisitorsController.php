<?php
namespace App\Http\Controllers;
use Storage;
use App\Visitors;
use App\VisitorLogs;
//use App\conversion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use GeoIp2\Database\Reader;

class VisitorsController extends Controller{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct(){
    //
  }
  public function liveTracking(Request $request){
    if( isset($request->e_c) && $request->e_c == 'popup' && isset($request->e_a) && !empty($request->e_a) ){
      $visitor_client_id  = $request->_id;
      $fid                = $request->idsite;
      $ip                 = $_SERVER['REMOTE_ADDR'];    
      $visitor            = Visitors::where('visitor_client_id',$visitor_client_id)->first();
      if( !$visitor ){
        $data       = ['visitor_client_id'=> $visitor_client_id,'client_ip'=>$_SERVER['REMOTE_ADDR']];
        $visitor    = Visitors::create($data);
        $data       = ['visitor_id'=> $visitor->visitor_id,'url'=> $request->url,'popup_id'=>$request->e_a, 'domain' => $fid];
        $visitorlog = VisitorLogs::create($data);        
      }else{        
        $data       = ['visitor_id'=> $visitor->visitor_id,'url'=> $request->url,'popup_id'=>$request->e_a, 'domain' => $fid];
        $visitorlog = VisitorLogs::create($data);        
      }
    }
  }
  /*public function gettracking(Request $request){
    $funnel_card_id=$request->funnel_card_id;
    $visitorcount  = visitorlog::select('funnel_card_id','visitor_id')->where('funnel_card_id',$funnel_card_id)->distinct()->count('visitor_id');
    $viewcount=visitorlog::select('funnel_card_id','visitor_id')->where('funnel_card_id',$funnel_card_id)->count();
    $conversioncount  = conversion::select('funnel_card_id','conversion_id')->where('funnel_card_id',$funnel_card_id)->count();
    return response()->json(['status' =>'success', 'visitorcount'=>$visitorcount,'viewcount'=>$viewcount,'conversioncount'=>$conversioncount],200); 
  }*/
}
