<?php
namespace App\Http\Controllers;
use App\PopupTemplates;
use App\Popups;
use App\Domains;
use App\PopupDomainRelations;
// use App\visitor;
// use App\visitorlog;
// use App\conversion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Http\Response;
use InnoCraft\Experiments\Experiment;
use Validator;
use Log;
use DB;

class RenderController extends Controller{
	private $autocomplete = false;
  	private $groupElementPrefix = '';
  	private $phonestyle="";
	private $tabletstyle="";
	private $screenstyle="";
	private $closeButton	= "";
	private $popupId		= "";	

	public function generateUserDomainPopup( Request $request ){
		$this->validate($request, [
			'user_id'	=> 'required',
			'domain'		=> 'required',
		]);
		try{
			$popupElements	= [];
			$domainIds		= Domains::where('user_id', $request->user_id)->where('domain_name', $request->domain)->pluck('domains_id')->toArray();
			if( count($domainIds) == 0 ){
				return response()->json([ 'status' => 'failed', 'message' => 'Domain not found for the user'], 200);
			}else{
				$popupIds	= PopupDomainRelations::whereIn('domains_id', $domainIds)->pluck('popup_id')->toArray();
				if( count($popupIds) == 0 ){
					return response()->json([ 'status' => 'failed', 'message' => 'No campaign related to the domain'], 200);
				}else{
					foreach( $popupIds as $key => $popupId ){
						$this->popupId	= $popupId;
						$popupTrigger	= '';
						$popupHtml		= '';
						$popup			= Popups::find($popupId);
						$popupJson		= $popup->popup_json_code;
						$popupTrigger	= $popup->trigger;
						$popupHtml		= $this->PopupGenerate($popupJson);
						//if( $popupTrigger && $popupHtml ){
							$popupElements[$popupId]['popupTrigger']	= $popupTrigger;
							$popupElements[$popupId]['popupHtml']		= $popupHtml;
						//}
					}
				}
			}
			return response()->json(['status' => 'success', 'message' => 'Popup HTML & Triggered Reterived Succesfully', 'popupElements' => $popupElements], 200);
		}catch(Exception $e){
			Log::info($e); 
			return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
		}
		
	}

	public function PopupGenerate( $popupJson ){
		$pop_up_json_code	= json_decode($popupJson);
		$popup				= "";
		$width				= "";
		$height				= "";
		$sections			= "";
		foreach ($pop_up_json_code as $key => $value){
			switch ($key){
				case "popup":
					$this->closeButton	= $value->closeButton;
					if($this->closeButton==true){
						$this->closeButton	= '';
					}else{
						$this->closeButton	= 'style="display:none"';
					}
					$overlayColor		= $value->overlayColor;
					$startTime			= $value->startTime;					
					$width				= "";
					$height				= "";
					$backgroundColor	= "";
					$backgroundImage	= "";
					$paddingTop			= "";
					$paddingRight		= "";
					$paddingBottom		= "";
					$paddingLeft		= "";
					$paddingLeft		= "";
					if( isset($value->popupStyle) ){
						$popupStyle	= $value->popupStyle;
						if( isset($popupStyle->width) ){
							$width	= $popupStyle->width;
						}
						if( isset($popupStyle->height) ){
							$height	= $popupStyle->height;
						}
						if( isset($popupStyle->backgroundColor) ){
							$backgroundColor	= $popupStyle->backgroundColor;
						}
						if( isset($popupStyle->backgroundImage) ){
							$backgroundImage	= "".$popupStyle->backgroundImage."";
						}
						if( isset($popupStyle->paddingTop) ){
							$paddingTop       = $popupStyle->paddingTop;
						}
						if( isset($popupStyle->paddingRight) ){
							$paddingRight     = $popupStyle->paddingRight;
						}
						if( isset($popupStyle->paddingBottom) ){
							$paddingBottom    = $popupStyle->paddingBottom;
						}
						if( isset($popupStyle->paddingLeft) ){
							$paddingLeft      = $popupStyle->paddingLeft;
						}
					}
					$fullScreenActive		= "";
					$fullScreenDeactive	= "";
					if( $value->isFullPage === true ){
						$fullScreenActive	= 'width:auto;height:'.$height.'px;background-color:'.$backgroundColor.';background-image:url('.$backgroundImage.'); padding-top:'.$paddingTop.'px;padding-bottom:'.$paddingBottom.'px;padding-right:'.$paddingRight.'px;padding-left:'.$paddingLeft.'px;background-position: right bottom; background-repeat: no-repeat; background-size: 50%;';
					}else{
						$fullScreenDeactive	= 'background-image:url('.$backgroundImage.');';						
					}
					$popup = $popup."<div  class='popup-canvas'  style='".$fullScreenActive."'> <button id = 'x' onclick='popupclosebtn()' type='button' ".$this->closeButton." class='close' data-dismiss='modal' >&times;</button><div class='main-content'><div class='popup-container' style='width: ".$width."px;".$fullScreenDeactive."'><div class='popup-content' style='width:100%;'>";
					break;
				case "sections":
					foreach( $value as $key1 => $value1 ){
						$sections=$sections.$this->popupssectiongenerator($value1);
					}
					break;
			}
		}
		$popup		= $popup.$sections."</div></div></div></div>";
		$popuphead	= "<style>
							.popup-canvas { left: 0; bottom: 0; top: 0px; right:0px; position: fixed; padding: 50px 0; overflow-y: auto; overflow-x: hidden;}
							.popup-canvas::-webkit-scrollbar { width: 2px; }
							.popup-canvas::-webkit-scrollbar-thumb { background: #888; }
							.popup-canvas .main-content { padding: 2px 2px; position: relative; }
							.popup-canvas .main-content .popup-container { width: 100%; margin: 0 auto;display: flex; }							
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper { display: flex; height: auto; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column { flex-grow: 1; position: relative; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element-wapper { min-height: 100px; height: 100%; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element { border: 1px dashed transparent; display: inline-block; position: relative; width: 100%; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element p { margin: 0; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element.element-editable { z-index: 1; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element .popup-button-element { display: inline-block; margin: 0 auto; cursor: text; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element .popup-form .popup-form-input { width: 100%; padding: 10px; margin-bottom: 5px; }
							.popup-canvas .main-content .popup-container .popup-content .popup-column-wapper .popup-column .popup-element .popup-form .popup-form-submit { padding: 10px; display: block; margin: 0 auto; }
							.popup-content .popup-section > .popup-section-wrapper { position: relative; }
							.popup-structure { position: relative; }
							.popup-container .popup-container-wrapper { position: relative;
							display: inline-block; }
							.popup-element { word-break: break-word; }
							.popup-element p { margin: 0; }
							.popup-element .popup-element-wrapper { position: relative; }
							.popup-element .popup-element-wrapper hr { border-right: 0px !important; border-bottom: 0px !important; border-left: 0px !important; }							
							.popup-section{
							max-width:100%;
							}
							.popup-content {							
							overflow: hidden;
							}
							#x {
							position: absolute;
							top: 7px;
							right: 7px;
							border-radius: 25px;
							padding: 3px 7px;
							text-shadow: none;
							background: #eae8e8; 
							color: #020d3f;
							font-weight: 100;
							opacity: 0.4;
							border: 0px; 
							}
							#x:hover,  #x:focus,  #x:active{
							opacity: 1;
							outline:0;
							}
							.exit-modal
							{
							display:none;
							}

							</style>";
		return $popuphead.$popup."</div></div>";
	}
	
	public function popupssectiongenerator($json){
		$style				= "";
		$structure			= "";
		$structureStyle	= "";
		$structureNew		= "";		
		if( isset($json->style) ){
			foreach ($json->style as $key => $value) {				
				$style		.= $this->popupssetstyle($key,$value);
			}
		}
		if( isset($json->structures) ){
			foreach( $json->structures as $key => $value ){
				$structure=$structure.$this->popupsstructuegenerator($value);		
			}
		}
		return '<div class="popup-section" id="'.$json->id.'" style="'.$style.'"><div class="popup-section-wrapper" >'.$structure.'</div></div>';
	}

	public function popupsstructuegenerator($json){
		$column	= '';
		$style	= '';
		if( isset($json->style) ){
			foreach( $json->style as $key => $value ){
				$stylevar	=	$key;
				$stylevar	= $value;
				$style		= $style.$this->popupssetstyle($key,$value);
			}
		}
		foreach ($json->columns as $key => $value) {
			$column=$column.$this->popupscolumngenerator($value);
		}	
		if(isset($json->width)){
			$width	= $json->width;
		}
		return '<div class="popup-structure-wrapper" style="'.$style.'"><div style="width: 100%;"><div class="popup-structure" style="margin: 0px auto; width: 100%;"><div class="popup-structure-inside" ><div class="popup-structure-wrapper"><div class="popup-container" >'.$column.'</div></div></div></div></div></div>';
	}

	public function popupscolumngenerator($jsons){
		$containers	= '';
		$width		= '';
		if( isset($jsons->width) ){
			$width=$jsons->width;
		}
		$style	= "";
		foreach( $jsons->containers as $key => $value ){
			if( isset($value->style) ){
				foreach ($value->style as $key => $value1){
					$stylevar	= $key;
					$stylevar	= $value;
					$style		= $style.$this->popupssetstyle($key,$value1);
				}
			}
			$containers	.= '<div class="popup-container-wrapper" style="padding:5px;width:'.$width.'%;'.$style.'"><div width="100%"><div class="popup-element-wapper">'.$this->popupscontainergenerator($value).'</div></div></div>';
		}
		return $containers;
	}
	
	public function popupscontainergenerator($json){
		$elements	= '';
		$width		= '';
		$style		= '';
		if( isset($json->style) ){
			foreach( $json->style as $key => $value ){				
				$style	.= $this->popupssetstyle($key,$value);
			}
		}
		foreach( $json->elements as $key => $value ){
			$elements	.= '<div class="popup-element"><div class="popup-element-wrapper">'.$this->popupselementgenerator($value).' </div></div>';
		}
		return $elements;
	}

	public function popupselementgenerator($json){  
		$btnclick		= "";
		$style			= "";
		$contents		= '';
		$buttonstyle	= '';
		switch( $json->type ){
			case "IMAGE":
				if( isset($json->style) ){
					foreach( $json->style as $key => $value ){				
						$style	.= $this->popupssetstyle($key,$value);
					}
				}
				if( isset($json->url) ){
					$path	= $json->url;
				}

				if( isset($json->alt) ){
					$alt = $json->alt;
				}
				return  '<div style="'.$style.'width: auto !important"><img src="'.$path.'" alt="'.$alt.'" width="'.$json->style->width.'%"/></div>';
				break;
			case "TEXT":
				if( isset($json->style) ){
					foreach( $json->style as $key => $value ){			
						$style	.= $this->popupssetstyle($key,$value);
					}
				}
				return  '<div style="'.$style.'"> <div class="text-element">'.$json->content.'</div></div>';
				break;			
			case "BUTTON":
				if( isset($json->buttonStyle) ){
					foreach( $json->buttonStyle as $key => $value ){
						$buttonstyle	.= $this->popupssetstyle($key,$value);
						if($key == "textAlign"){
							$style	.= $this->popupssetstyle($key,$value);  
						}
					}
				}
				if( isset($json->style) ){
					foreach( $json->style as $key => $value ){			
						$style	.= $this->popupssetstyle($key,$value);
					}
				}
				if( isset($json->actionType) ){
					if( $json->actionType == "popup" ){
						$btnclick = "onclick='popupview()'";
					}
				}
				$setcontents = $json->content;
				if( isset($json->actionUrl) ){			
					$setcontents = '<a href="'.$json->actionUrl.'" style="color:inherit;">'.$json->content.'</a>';
				}
				return  '<div style="'.$style.';"><button style="'.$buttonstyle.'text-align:center;">'.$setcontents.'</button></div>';
				break;
			case "VIDEO":
				if( isset($json->height) ){
					$height	= $json->height;
				}else{
					$height	= '';
				}
				if( isset($json->width) ){
					$width	= $json->width;
				}else{
					$width	= '';
				}
				return  '<div style="'.$style.'"><span><img src="'.$json->url.'"></span></div>';
				break;
			case "SPACE":
				if( isset($json->style) ){
					foreach( $json->style as $key => $value ){			
						$style	.= $this->popupssetstyle($key,$value);
					}
				}
				return  '<div style="'.$style.'"></div>';
				break;
			case "LINE":
				if( isset($json->style) ){
					foreach ($json->style as $key => $value) {
						$style	.= $this->popupssetstyle($key,$value);
					}
				}
				return  '<div><hr style="'.$style.'"></div>';
				break;
			case "FORM":
				if( isset($json->style) ){
					foreach( $json->style as $key => $value ){
						$style	.= $this->popupssetstyle($key,$value);
					}
				}
				if( isset($json->fontSize) && !empty($json->fontSize) ){
					$style	.= $this->popupssetstyle("font-size", $json->fontSize."px");
				}
				if( isset($json->fontFamily) && !empty($json->fontFamily) ){
					$style	.= $this->popupssetstyle("font-family", $json->fontFamily);
				}
				if( isset($json->lineHeight) && !empty($json->lineHeight) ){
					$style	.= $this->popupssetstyle("line-height", $json->lineHeight."px");
				}
				if( isset($json->fontColor) && !empty($json->fontColor) ){
					$style	.= $this->popupssetstyle("color", $json->fontColor);
				}
				$actionUrl  = '';
				if( isset($json->actionUrl) && !empty($json->actionUrl) ){
					$actionUrl  = "<input type='hidden' name='actionUrl' id='actionUrl' value='".$json->actionUrl."' />";
				}
				$formName  = '';
				if( isset($json->formName) && !empty($json->formName) ){
					$formName = ' name="'.str_replace(" ","_",$json->formName).'" ';
				}          
				$popupForm  = $this->advancedFormGenerator($json->rows, 'popup');
				$formHtml   = '<form class="popup-form" '.$formName.' id="popupform" style="'.$style.'">'.$actionUrl.$popupForm.'<input type="hidden" name="popup_id" value="'.$this->popupId.'"></form>';
				return $formHtml;
				break;			
		}
	}

	public function popupssetstyle($stylevar,$styleval){
		switch ($stylevar) {
			case "backgroundColor":
				return  "background-color:".$styleval.";";
				break;
			case "margin":
				return  "margin:".$styleval.";";
				break;
			case "backgroundImage":
				return  "background-image:url(".$styleval.");";
				break;
			case "textAlign":
				return  "text-align:".$styleval.";";
				break;
			case "marginLeft":
				return  "margin-left:".$styleval.";";
				break;
			case "marginRight":
				return  "margin-right:".$styleval."px;";
				break;
			case "marginTop":
				return  "margin-top:".$styleval."px;";
				break;
			case "marginBottom":
				return  "margin-bottom:".$styleval."px;";
				break;
			case "paddingLeft":
				return  "padding-left:".$styleval."px;";
				break;
			case "paddingRight":
				return  "padding-right:".$styleval."px;";
				break;
			case "paddingTop":
				return  "padding-top:".$styleval."px;";
				break;
			case "paddingBottom":
				return  "padding-bottom:".$styleval."px;";
				break;
			case "backgroundRepeat":
				return  "background-repeat:".$styleval.";";
				break;
			case "backgroundPosition":
				return  "background-position:".$styleval.";";
				break;
			case "backgroundSize":
				return  "background-size:".$styleval.";";
				break;
			case "fontFamily":
				return  "font-family:".$styleval.";";
				break;
			case "fontSize":
		      return  "font-size:".$styleval."px;";
		      break;
			case "fontColor":
				return  "color:".$styleval.";";
				break;
			case "color":
				return  "color:".$styleval.";";
				break;
			case "width":
				return  "width:".$styleval."%;";
				break;
			case "borderBottomStyle":
				return  "border-bottom-style:".$styleval.";";
				break;
			case "borderTopStyle":
				return  "border-top-style:".$styleval.";";
				break;
			case "borderRightStyle":
				return  "border-right-style:".$styleval.";";
				break;
			case "borderLeftStyle":
				return  "border-left-style:".$styleval.";";
				break;
			case "borderBottomColor":
				return  "border-bottom-color:".$styleval.";";
				break;
			case "borderTopColor":
				return  "border-top-color:".$styleval.";";
				break;
			case "borderRightColor":
				return  "border-right-color:".$styleval.";";
				break;
			case "borderLeftColor":
				return  "border-left-color:".$styleval.";";
				break;
			case "borderBottomWidth":
				return  "border-bottom-width:".$styleval."px;";
				break;
			case "borderTopWidth":
				return  "border-top-width:".$styleval."px;";
				break;
			case "borderRightWidth":
				return  "border-right-width:".$styleval."px;";
				break;
			case "borderLeftWidth":
				return  "border-left-width:".$styleval."px;";
				break;
			case "borderRadius":
				return  "border-radius:".$styleval."px;";
				break;
			case "borderWidth":
				return  "border-width:".$styleval."px;";
				break;
			case "borderColor":
				return  "border-color:".$styleval.";";
				break;
			case "borderStyle":
				return  "border-style:".$styleval.";";
				break;
			case "fontSize":
				return "font-size:".$styleval."px;";
				break;
			case "height":
				if($styleval){
					return  "height:".$styleval."px;";
				}else{
					return  "height:auto;";
				}
				break;
			default:
				return  $stylevar.":".$styleval.";";
				break;
		}
	}

	public function advancedFormGenerator( $json, $type = 'pages' ) {
		$this->autocomplete			= false;
		$this->groupElementPrefix	= '';
		$jsonObj    = $json;
		$formHtml   = "";    
		foreach( $jsonObj as $key => $row ){      
			$column = '';
			foreach( $row->cols as $key1 => $col ){
				$column .= $this->formColumnGenerator($col);
			}
			$formHtml .= "<div class='row'>".$column."</div>";
		}
		return $formHtml;
	}

	public function formColumnGenerator($json){    
		$size         = $json->size;
		$class        = $this->bootstrapColClassGenerator($size);
		$elementHtml  = "";
		foreach ($json->elements as $key => $element ) {
			$elementHtml    .= $this->formElementGenerator($element); 
		}
		$colStyle   = '';
		if( isset($json->style) ){
			foreach ($json->style as $key => $value) {
				$colStyle   .= $this->popupssetstyle($key,$value);
			}
		}
		$column = "<div class='".$class."' style='".$colStyle."'>".$elementHtml."</div>";
		return $column;
	}

	public function bootstrapColClassGenerator( $size ){
		$class  = "";
		$resCol = array("lg" => "xl-","md" => "lg-", "sm" => "md-", "xs" => '');
		if( is_object($size) ){      
			foreach ( $size as $key => $value ){
				$class .= " col-".$resCol[$key].$value;
			}
		}else{
			$class  = "col-sm-".$size." col-12";
		}
		return $class;
	}

	public function formElementGenerator( $elementObj ){
		$fieldClass   = $labelClass = '';
		if( isset($elementObj->labelSizes) ){
			$labelClass   = $this->bootstrapColClassGenerator($elementObj->labelSizes);
		}
		if( isset($elementObj->fieldSizes) ){
			$fieldClass   = $this->bootstrapColClassGenerator($elementObj->fieldSizes);
		}
		$autocomplete   = '';
		if( $this->autocomplete ){
			$autocomplete = 'autocomplete="off"';
		}
		$jsonId       = $elementObj->id;
		$labelHtml    = "";
		$prependHtml  = "";
		$fields       = "";
		if( isset($elementObj->isLabel) && $elementObj->isLabel && isset($elementObj->label) && $elementObj->label ){
			$labelStyle   = '';
			if( isset($elementObj->labelCol) ){
				foreach ($elementObj->labelCol as $key => $value) {
					$labelStyle   .= $this->popupssetstyle( $key, $value);
				}
			}
			$labelHtml    = "<div class='".$labelClass."' style='".$labelStyle."'><label class='form-label' for='".$jsonId."'>".$elementObj->label.($elementObj->isRequired ? "<em style='color: red;'>*</em>" : "")."</label></div>";
		}
		if(isset($elementObj->prepend) && $elementObj->prepend ){
			if(isset($elementObj->prependIcon)){
				$prependHtml  = "<div class='input-group-prepend'><span class='input-group-text'><i class='fas ".$elementObj->prependIcon."' aria-hidden='true'></i></span></div>";
			}else{
				$prependHtml  = "<div class='input-group-prepend'><span class='input-group-text'>".$elementObj->prependText."</span></div>";
			}
		}
		$fields   = $elementObj->fieldType;
		if( isset($elementObj->placeholder) ){
			$placeholder  = $elementObj->placeholder;
		}else{
			$placeholder  = '';
		}
		if( isset( $elementObj->isRequired ) && $elementObj->isRequired ){
			$required = " required ";
		}else{
			$required = '';
		}
		$inputStyle = "";
		if( isset($elementObj->controlStyle) && is_object($elementObj->controlStyle)){
			foreach ($elementObj->controlStyle as $key => $value) {
				$inputStyle .= $this->popupssetstyle($key, $value);
			}
		}
		$name = '';
		if( $elementObj->fieldType <> 'submit' ){
			$name = $this->groupElementPrefix.$elementObj->label;
		}
		switch ( $elementObj->fieldType ) {
			case 'text':
				$fields = "<input type='text' ".$autocomplete." name='".$name."' class='form-control' id='".$jsonId."' placeholder='".$placeholder."' ".$required." style='".$inputStyle."' />";
				break;
			case 'date':
				$fields = "<input type='date' name='".$name."' class='form-control' id='".$jsonId."' placeholder='".$placeholder."' ".$required." style='".$inputStyle."' />";
				break;
			case 'textarea':
				$rows = 3;
				if( isset($elementObj->rows) && !empty($elementObj->rows) ){
					$rows = $elementObj->rows;
				}
				$fields = "<textarea name='".$name."' class='form-control' id='".$jsonId."' placeholder='".$placeholder."' ".$required." rows='".$rows."' style='".$inputStyle."'></textarea>";
				break;
			case 'email':
				$fields = "<input name='email' type='email' class='form-control' id='".$jsonId."' placeholder='".$placeholder."'  ".$required."  style='".$inputStyle."' />";
				break;
			case 'number':
				$fields = "<input type='number' name='".$name."' class='form-control' id='".$jsonId."' placeholder='".$placeholder."' ".$required."  style='".$inputStyle."' />";
				break;
			case 'tel':
				$fields = "<input type='tel' name='".$name."' class='form-control' id='".$jsonId."' placeholder='".$placeholder."' ".$required."  style='".$inputStyle."' />";
				break;
			case 'checkbox':
				$checkbox = '';
				foreach( $elementObj->options as $option ){
					$checkbox .= "<div class='form-check form-check-inline'><input type='checkbox'  value='".$option."' name='".$name."[]'  ".$required." class='form-check-input' /> <label class='form-check-label'  style='".$inputStyle."'>".$option."</label></div>";
				}
				$fields = $checkbox;
				break;
			case 'radio':
				$radio	= '';    
				foreach($elementObj->options as $option){
					$radio .=  "<div class='form-check form-check-inline'><input type='radio' value='".$option."' name='".$name."'  ".$required." class='form-check-input' /> <label class='form-check-label'  style='".$inputStyle."'>".$option."</label></div>";
				}
				$fields = $radio;
				break;
			case 'select':
				$select	= '';
				if( isset( $elementObj->placeholder ) ){    
					$optionplaceholder = "<option  value='' > ".$elementObj->placeholder."</option>";
				}else{
					$optionplaceholder = '';
				}
				foreach($elementObj->options as $option){
					$select .= "<option  value='".$option."' > ".$option."</option>";
				}
				$fields = "<select class='form-control' name='".$name."' ".$required."  style='".$inputStyle."'>".$optionplaceholder.$select."</select>";
				break;
			case 'file':
				$fileTypes = "";
				if(isset($elementObj->fileTypes)){
					if(sizeof($elementObj->fileTypes) > 0){
						$imageformat = array("png","jpg","jpeg");
						$imgfor      = array();
						$filefor     = array();
						foreach($elementObj->fileTypes as $rows){
							if(in_array($rows, $imageformat)){
								$imgfor[] = "image/".$rows;
							}else{
								$filefor[] = "application/".$rows;
							}
						}
						$setimg = "";
						if(sizeof($imgfor) > 0){
							$setimg = implode(',',$imgfor);
						}
						if(sizeof($filefor) > 0){
							$setimg = $setimg.implode(',',$filefor);
						}
						$fileTypes  = 'accept="'.$setimg.'"';
					}
				}
				$multiFile = "";
				if($elementObj->multiFile ==true){
					$multiFile = "multiple";
				}
				$fields = "<div class='file-upload-group'><div class='file-upload-fake'><i class='material-icons'>cloud_upload</i><span>Drop Here</span></div><input style='".$formInputBorderRadius."' type='file'  name='files[]' id='files' ".$required." ".$fileTypes." class='file-upload' ".$multiFile."></div><pre id='filelist' style='display:none;'></pre>";
				break;
			case 'submit':				
				$style	= $this->setscreenstyle($elementObj->buttonStyle);        
				$fields	= "<div class='form-button' style='text-align:".$elementObj->buttonCol->textAlign."'><button type='submit' class='btn btn-".$elementObj->color."' style='".$style."'>".$elementObj->buttonText."</button></div>";
				break;
			default:
				$fields = '';
				break;
		}    
		if( $elementObj->fieldType != "submit" && $elementObj->fieldType != "radio" && $elementObj->fieldType != "checkbox" ){      
			$fields = "<div class='input-group'>".$prependHtml.$fields."</div>";
		}
		$fieldStyle   = '';
		if( isset($elementObj->controlCol) ){
			foreach( $elementObj->controlCol as $key => $value ){
				$fieldStyle   .= $this->popupssetstyle( $key, $value );	
			}				
		}
		return  "<div class='row form-group'>".$labelHtml."<div class='".$fieldClass."' style='".$fieldStyle."'>".$fields."</div></div>";		
	}	
	
	public function setscreenstyle($styles){
		$cssvalue='';  
		foreach ($styles as $key => $value) {    
			switch ($key) {
				case 'background':
					$cssvalue	.= "background-color:".$value.';';
					break;
				case 'paddingTop':
					$cssvalue	.= "padding-top:".$value.'px;';
					break;
				case 'paddingBottom':
					$cssvalue	.= "padding-bottom:".$value.'px;';
					break;
				case 'paddingLeft':
					$cssvalue	.= "padding-left:".$value.'px;';
					break;
				case 'paddingRight':
					$cssvalue	.= "padding-right:".$value.'px;';
					break;
				case 'marginTop':
					$cssvalue	.= "margin-top:".$value.'px;';
					break;
				case 'marginBottom':
					$cssvalue	.= "margin-bottom:".$value.'px;';
					break;
				case 'marginLeft':
					$cssvalue	.= "margin-left:".$value.'px;';
					break;
				case 'marginRight':
					$cssvalue	.= "margin-right:".$value.'px;';
					break;
				case 'backgroundImage':
					$cssvalue	.= "background-image:url(".$value.");";
					break;
				case 'textAlign':
					$cssvalue	.= "text-align:".$styleval.";";
					break;
				case 'fontFamily':
					$cssvalue	.= "font-family:".$value.";";
					break;
				case 'display':
					$cssvalue	.= "display:".$value.";";
					break;
				case 'borderRadius':
					$cssvalue	.= "border-radius:".$value."px;";
					break;
				case 'width':
					$cssvalue	.= "width:".$value.";";
					break;
				case 'backgroundColor':
					$cssvalue	.= "background-color:".$value.";";
					break;
				case 'borderBottomStyle':
					$cssvalue	.= "border-bottom-style:".$value.";";
					break;
				case 'borderBottomColor':
					$cssvalue	.= "border-bottom-color:".$value.";";
					break;
				case 'borderBottomWidth':
					$cssvalue	.= "border-bottom-width:".$value."px;";
					break;
				case 'borderTopStyle':
					$cssvalue	.= "border-top-style:".$value.";";
					break;
				case 'borderTopColor':
					$cssvalue	.= "border-top-color:".$value.";";
					break;
				case 'borderTopWidth':
					$cssvalue	.= "border-top-width:".$value."px;";
					break;
				case 'borderLeftStyle':
					$cssvalue	.= "border-left-style:".$value.";";
					break;
				case 'borderLeftColor':
					$cssvalue	.= "border-left-color:".$value.";";
					break;
				case 'borderLeftWidth':
					$cssvalue	.= "border-left-width:".$value."px;";
					break;
				case 'borderRightStyle':
					$cssvalue	.= "border-right-style:".$value.";";
					break;
				case 'borderRightColor':
					$cssvalue	.= "border-right-color:".$value.";";
					break;
				case 'borderRightWidth':
					$cssvalue	.= "border-right-width:".$value."px;";
					break;
				case 'color':
					$cssvalue	.= "color:".$value.";";
					break;
				case 'fontSize':
					$cssvalue	.= "font-size:".$value."px;";
					break;
			}
		}
		return $cssvalue;
	}
}
?>