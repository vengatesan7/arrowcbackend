<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\TestEmail;

class MailConfigcontroller extends Controller
{
  
   public function forgotpasswordmail($user)
      {
          $user['subject'] = 'Arrowcampaign User Credentials';
          Mail::send('emails.usercreations', $user, function($message) use ($user) {
              $message->to($user['email']);
              $message->subject($user['subject']);
          });
         
      }

}