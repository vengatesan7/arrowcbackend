<?php
namespace App\Http\Controllers;
use App\User;
use App\role_master;
use App\smtp_mail_config;
use App\Channels;
use App\ChannelAssigns;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use PHPUnit\Exception;
use Validator;
use Illuminate\Support\Str;
use Spatie\Async\Pool;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\MailConfigcontroller;
use DB;

class UserController extends MailConfigcontroller{

    public $successStatus = 200;


    protected function jwt(user $user) {

        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->user_id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 * 60 * 24 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));

    }
    protected function jwt1($user) {

        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user['user_id'], // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 * 60 * 24 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));

    }

    public function validateToken(Request $request){
        return response()->json(['data'=>$request->auth], 200);
     }

    public function test(){
        return response()->json(['status' =>'success', 'message'=> 'Test function invoked successfully'],200);
    }
    /**
     * @OA\Get(
     *     path="/user/roletypemaster",
     *     operationId="/roletypemaster",
     *     tags={"Invite Team Members"},
     *     security={{"bearerAuth":{}}},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="User  ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *   ),
     *
     *     @OA\Response(
     *         response="200",
     *         description="Role master retrieved Successfully",
     *         ),
     *     @OA\Response(
     *         response="520",
     *         description="Application Error - Please see the log for more Information.",
     *     ),
     * )
     */
    public function roletypemaster(Request $request){
       
        try{
           $role_master  = role_master::select('role_id','name')->get();

            return response()->json(['status' =>'success', 'message'=> 'Role master retrieved successfully','role_master'=>$role_master],200);
        }catch(Exception $e)
        {
            //Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }

    }
     public function rolecreate(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'name' => 'required',
        ]);
       try{
            $role_master = new role_master;
            $role_master->name = $request->name;
            $role_master->save();
            return response()->json(['status' =>'success', 'message'=> 'Role Created successfully'],200);
        }catch(Exception $e)
        {
            Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
     /**
     * @OA\Post(
     *     path="/user/login",
     *     operationId="/userlogin",
     *     tags={"User Login"},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"email","password"},
     *         @OA\Property(
     *           property="email",
     *           description="email address of the  user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="password",
     *           description="password of the  user.",
     *           type="string",
     *         ),
     *      ),
     *    ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="Successfully Logged in",
     *  @OA\MediaType(
     *       ),
     *     ),
     * )
     */
    public function authenticateUser(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        $email_id = $request->input('email');
        $password = $request->input('password');
        
        $user = user::leftJoin('role_masters', function($join) {
         $join->on('users.role_id', '=', 'role_masters.role_id');
        })->where('email', trim($email_id))->first([
        'users.user_id',
        'users.first_name',
        'users.last_name',
        'users.email',
        'users.password',
        'role_masters.name as rolename',
        'users.role_id'
         ]);
        if($user && Hash::check(trim($password),$user->password)){
            $user_id    = $user->user_id;
            $channelIds = ChannelAssigns::where('user_id', $user_id)->orWhere('created_user_id', $user_id)->pluck('channel_id')->toArray();
            if( is_array($channelIds) && count($channelIds) == 0 ){
                $channelIds = Channels::where('created_user_id', $user_id)->pluck('channel_id')->toArray();
            }
            $user_data = ['user_id' => $user->user_id,'email' => $user->email,'first_name'=>$user->first_name,'last_name'=>$user->last_name,'rolename'=>$user->rolename,'role_id'=>$user->role_id , 'no_of_channels' => count($channelIds), 'channel_ids' => $channelIds];
            return response()->json(['status' =>'success', 'message'=> 'Login Successful','token' => $this->jwt($user),'user_data'=>$user_data],200);

        }else{
            return response()->json(['status' => 'failed','message' => 'Invalid Login'],200);
        }
    }
    /**
     * @OA\Post(
     *     path="/user/create",
     *     operationId="/createUser",
     *     tags={"Create New User"},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"email","first_name"},
     *         @OA\Property(
     *           property="email",
     *           description="email address of the new user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="first_name",
     *           description="First Name of the new user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="last_name",
     *           description="Last name of the new user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="mobile_number",
     *           description="mobile number of the new user.",
     *           type="string",
     *         ),
     *       ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="User Created Successfully",
     *         ),
     *     @OA\Response(
     *         response="520",
     *         description="Application Error - Please see the log for more Information.",
     *     ),
     * )
     */

    public function createUser(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'first_name' => '',
            'last_name' => '',
            'password' => '',
            'mobile_number' => '',
            'role_id'=>'required'
        ]);
        try{
            //$user_id   = $request->auth->user_id;
            if($request->input('password') ==""){$request->password      = str::random(10);}
            $f = explode('@',$request->input('email'));
            $user = new user;
            $user->email = $request->input('email');
            $user->password = Hash::make($request->password);
            $user->first_name = $f[0];
            $user->last_name = $f[0];
            $user->mobile_number = $request->input('mobile_number');

            $user->user_token = base64_encode(Str::random(10));
            $user->ip_address = $_SERVER['REMOTE_ADDR'];
            $user->role_id = $request->role_id;
            $user->save();

            $user_data = ['email' => $request->input('email'),'first_name'=>$user->first_name,'last_name'=>$user->last_name,'password'=>$request->password,'user_id'=>$user->user_id];
            $this->forgotpasswordmail($user_data); 
            $user_data['password'] = "";
            $user_data['role_id'] = $request->role_id;
            return response()->json(['status' =>'success', 'message'=> 'User Created successfully','user_data'=>$user_data,'token' => $this->jwt($user)],200);
        }catch(Exception $e)
        {
            //Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }

    }
    public function subuserscreate(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'first_name' => '',
            'last_name' => '',
            'mobile_number' => '',
            'role_id'=>'required'
        ]);
        try{
            $user_id   = $request->auth->user_id;
            
            $pwdstr      = str::random(10);
            $password = Hash::make($pwdstr);

            $user = new user;
            $user->email = $request->input('email');
            $user->password = $password;
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->mobile_number = $request->input('mobile_number');

            $user->user_token = base64_encode(Str::random(10));
            $user->ip_address = $_SERVER['REMOTE_ADDR'];
            $user->role_id = $request->role_id;
            $user->created_user_id =  $user_id;
            $user->save();

            $user_data = ['email' => $request->input('email'),'first_name'=>$request->input('first_name'),'last_name'=>$request->input('last_name'),'password'=>$pwdstr];
            $this->forgotpasswordmail($user_data); 

            return response()->json(['status' =>'success', 'message'=> 'User Created successfully','user_data'=>$user_data],200);
        }catch(Exception $e)
        {
            //Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }

    }
    
    public function channellist(Request $request){
        $user_id = $request->auth->user_id;
        try {
           
          $channel  = Channels::select('*')->where('created_user_id',$user_id)->get();
            return response()->json(['status' => 'success','message'=>'Channel Fetched Successfully','channellist'=>$channel], 200);
        }catch(Exception $e){
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
    
    public function assignchannel(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'channel_id' => 'required',
            'campaign_id' => 'required',
            'user_id' => 'required',
        ]);
        try{
            $user_id   = $request->auth->user_id;
            $channel = new ChannelAssigns;
            $channel->user_id = $request->input('user_id');
             $channel->campaign_id = $request->input('campaign_id');
             $channel->channel_id = $request->input('channel_id');
            $channel->created_user_id =  $user_id;
            $channel->save();
           
            return response()->json(['status' =>'success', 'message'=> 'channel assign successfully'],200);
        }catch(Exception $e)
        {
            //Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }

    }
   
    public function getallsubusers(Request $request){
        $user_id = $request->auth->user_id;
        try {
           
            $user = DB::table('users')->select('users.user_id','users.first_name','users.last_name','users.email',
                'users.mobile_number','users.status', 'users.created_at','role_masters.name as rolename','users.role_id')
                ->leftjoin('role_masters', 'users.role_id', '=', 'role_masters.role_id')
                ->where('users.created_user_id','=',$user_id)
                ->get();
            return response()->json(['status' => 'success','message'=>'User Information Fetched Successfully','getallsubusers'=>$user], 200);
        }catch(Exception $e){
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
    public function getuserbyid(Request $request){
       $user_id = $request->auth->user_id;
        
        try {
            $user  = user::find($user_id);
            $user->password  = "";
            $user->address  = "";
            return response()->json(['status' => 'success','message'=>'User Information Fetched Successfully','userdata'=>$user], 200);
        }catch(Exception $e){
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
   
    /**
     * @OA\Post(
     *     path="/user/forgotpassword",
     *     operationId="/forgotpassword",
     *     tags={"Forgot password"},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"email"},
     *         @OA\Property(
     *           property="email",
     *           description="email address of the  user.",
     *           type="string",
     *         ),
     *      ),
     *    ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="password Reset Request has been sent",  
     *         ),
     *     ),
     * )
     */

        public function forgotpassword(Request $request){
                     $this->validate($request, [
            'email' => 'required|email'
                ]);
         try {  
           
             $user = user::where('email', $request->input('email'))->first();
             
             if($user->count()>0)
             {
                 $pwdstr      = str::random(10);
                 $password = Hash::make($pwdstr);
              
                $update = user::where('email',$request->email)->update(['password' => $password]);

                 $mailData['usertoken']=$user->user_token;
                $user_data = ['email' => $request->email,'first_name'=>$user->first_name,'last_name'=>$user->last_name,'password'=>$pwdstr];
                 
                 $this->forgotpasswordmail($user_data); 
                 return response()->json(['status' =>'success', 'message'=> 'Password Reset'],200); 
             }else{
                 return response()->json(['status' =>'success', 'message'=> 'Password Not Reset'],200); 
             }
              
            
        }
            catch(Exception $e)
            {
                Log::info($e); 
              return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
            }

        }
                /**
     * @OA\Put(
     *     path="/user/updatepassword",
     *     operationId="/user/updatepassword",
     *     tags={"Change password"},
     *  @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"password_Token","password"},
     *         @OA\Property(
     *           property="user_token",
     *           description="password Token of the  user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="password",
     *           description="password  of the  user.",
     *           type="string",
     *         ),
     *      ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="User password Updated Successfully",  

     *         ),
     *   
     * )
     */ 
    public function updateResetpassword(Request $request){
           $this->validate($request, [
            'password' => 'required',
            'user_token' => 'required'
        ]);
        try
            {
            $user = user::where('user_token', $request->input('user_token'))->first();
            $password = Hash::make($request->input('password'));
            $check_token = $request->input('user_token');
            if($user)
            {
                $update = user::where('user_token',$request->input('user_token'))->update(['password' => $password,'user_token' => null]);
                return response()->json(['status' =>'success', 'message'=> 'User password Updated Successfully'],200); 
            }
            else
            {
                return response()->json(['status' =>'failed', 'message'=> 'email is not avaiable'],200); 
            }
        }
        catch(\Exception $e)
        {
            Log::info($e); 
          return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);   
        }
      
    }

    /**
     * @OA\Put(
     *     path="/user/changepassword",
     *     operationId="/user/changepassword",
     *     tags={"Change password"},
     *      security={{"bearerAuth":{}}},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="User  ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"old_password","new_password"},
     *         @OA\Property(
     *           property="old_password",
     *           description="Old_password of the  user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="new_password",
     *           description="new_password address of the  user.",
     *           type="string",
     *         ),
     *      ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="password Updated Successfully",
     *         ),
     * )
     */
    public function changePassword(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'oldpassword' => 'required',
            'password' => 'required|min:5'
        ]);

        $user_id   = $request->auth->user_id;

        try{
            $user  = user::find($user_id);
            if(Hash::check($request->input('oldpassword'), $user->password))
            {
                $user->password = Hash::make($request->input('password'));
                $user->save();
                return response()->json(['status' => 'success','message' =>  'Password Updated Successfully'],200);
            }
            else
            {
                return response()->json(['status' => 'failed','message' =>  'Incorrect Old password'],200);
            }
        }catch(Exception $e){
            Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }

     

    /**
     * @OA\Post(
     *     path="/user/updateuserdetail",
     *     operationId="/user/updateUserDetail",
     *     tags={"Update User Details"},
     *     security={{"bearerAuth":{}}},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          required={"user_id","email","password"},
     *         @OA\Property(
     *           property="first_name",
     *           description="First Name of the user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="last_name",
     *           description="Last name of the user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="mobile_number",
     *           description="mobile number of the user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="address",
     *           description="address line of the user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="city",
     *           description="City of the user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="state",
     *           description="State of the user.",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="country_code",
     *           description="Country code of the user.",
     *           type="string",
     *         ),
     *       ),
     *     ),
     *   ),
     *     @OA\Response(
     *         response="200",
     *         description="User Updated Successfully",
     *         ),
     *     @OA\Response(
     *         response="520",
     *         description="Application Error - Please see the log for more Information.",
     *     ),
     *       ),
     *      ),
     *     ),
     * )
     */

    public function updateUserDetail(Request $request){
         $this->validate($request, [
            'user_id' => 'required'
        ]);
        try{
            $user_id   = $request->auth->user_id;
            $user  = user::find($user_id);

            if($request->input('first_name'))
            {
                $user->first_name = $request->input('first_name');
            }
            if($request->input('last_name'))
            {
                $user->last_name = $request->input('last_name');
            }
            if($request->input('mobile_number'))
            {
                $user->mobile_number = $request->input('mobile_number');
            }
            $user->save();
            return response()->json(['status' => 'success','message'=>'User Details Updated Successfully'], 200);

        }catch(Exception $e){
            Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);

        }
    }

    /**
     * @OA\Delete(
     *     path="/user/deleteUser",
     *     operationId="/deleteUser",
     *     security={{"bearerAuth":{}}},
     *     tags={"Delete User"},
     *  @OA\RequestBody(
     *     required=true,
     *     @OA\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="User  ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *   ),
     *
     *     @OA\Response(
     *         response="200",
     *         description="User Deleted Successfully",
     *         ),
     *     @OA\Response(
     *         response="520",
     *         description="Application Error - Please see the log for more Information.",
     *     ),
     * )
     */
    public function deleteUser(Request $request){
         $this->validate($request, [
            'user_id' => 'required'
        ]);
        try
        {
            $user  = user::find($request->user_id);
            $user->delete();
            return response()->json(['status' => 'success','message'=>'User Deleted Successfully'], 200);
        }
        catch(\Exception $e)
        {
            Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
    public function smtp_mail_save(Request $request){
        //Log::info($request);
        $this->validate($request, [
            'smtp_config_id' => '',
            'smtp_from_email' => 'required',
            'smtp_from_name' => 'required',
            'smtp_from_username' => 'required',
            'smtp_from_password' => 'required',
            'smtp_from_host'=>'required',
            'smtp_from_port'=>'required',
            'smtp_type'=>'required'
        ]);
        try{
            $user_id   = $request->auth->user_id;
            if($request->smtp_config_id > 0){
                $smtp = smtp_mail_config::find($request->smtp_config_id);
                $smtp->smtp_from_email          = $request->smtp_from_email;
                $smtp->smtp_from_name           = $request->smtp_from_name;
                $smtp->smtp_from_username       = $request->smtp_from_username;
                $smtp->smtp_from_password       = $request->smtp_from_password;
                $smtp->smtp_from_host           = $request->smtp_from_host;
                $smtp->smtp_from_port           = $request->smtp_from_port;
                $smtp->smtp_type                = $request->smtp_type;
                $smtp->created_user_id          = $user_id;
                $smtp->save();
            }else{
                $smtp = new smtp_mail_config;
                $smtp->smtp_from_email          = $request->smtp_from_email;
                $smtp->smtp_from_name           = $request->smtp_from_name;
                $smtp->smtp_from_username       = $request->smtp_from_username;
                $smtp->smtp_from_password       = $request->smtp_from_password;
                $smtp->smtp_from_host           = $request->smtp_from_host;
                $smtp->smtp_from_port           = $request->smtp_from_port;
                $smtp->smtp_type                = $request->smtp_type;
                $smtp->created_user_id          = $user_id;
                $smtp->save();
            }
           
            return response()->json(['status' =>'success', 'message'=> 'smtp created successfully'],200);
        }catch(Exception $e)
        {
            //Log::info($e);
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }

    }
    public function getsmtp_mail(Request $request){
        $user_id = $request->auth->user_id;
        try {
           
           $smtp_mail_config  = smtp_mail_config::select('*')->where('created_user_id',$user_id)->get();
           // $arr = array();
           // foreach ($smtp_mail_config->toArray() as $key => $value) {
           //          $value1[$value['smtp_type']]  =  $value;
           //          $arr[]         =  $value1;
           //  }
            return response()->json(['status' => 'success','message'=>'Smtp Fetched Successfully','getstmp'=>$smtp_mail_config], 200);
        }catch(Exception $e){
            return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
        }
    }
}

