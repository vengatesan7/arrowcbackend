<?php
	namespace App\Http\Controllers;
	use App\User;
	use App\smtp_mail_config;
	use App\Channels;
	use App\ChannelAssigns;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Hash;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Mail;
	use Firebase\JWT\JWT;
	use Firebase\JWT\ExpiredException;
	use PHPUnit\Exception;
	use Validator;
	use Illuminate\Support\Str;
	use Spatie\Async\Pool;
	use Illuminate\Support\Facades\Log;
	use App\Http\Controllers\MailConfigcontroller;
	use DB;
	class ChannelsController extends MailConfigcontroller{
		
		public function createChannel( Request $request ){
			$user_id = $request->auth->user_id;
			$this->validate($request, [
				'name' => 'required',
			]);
			try{
				if( is_array($request->name) ){
					$nameArray	= $request->name;
					foreach ($nameArray as $key => $value) {
						$dataParam	= [ 'created_user_id' => $user_id, 'name' => $value ];
						$channel		= Channels::create($dataParam);
					}					
				}else{
					$dataParam	= [ 'created_user_id' => $user_id, 'name' => $request->name ];
					$channel		= Channels::create($dataParam);
				}
				return response()->json(['status' =>'success', 'message'=> 'channel Created successfully', 'channel' => $channel],200);
			}catch(Exception $e){
				return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
			}
		}

		public function addUserToChannel( Request $request ){
			$user_id = $request->auth->user_id;
			$this->validate($request, [
				'channel_id'	=> 'required|integer|exists:channels',
				'name'			=> 'required',
				'email'			=> 'required',
			]);
			try{
				$user			= User::where('email', $request->email)->first();
				$channelId	= $request->channel_id;
				if( $user ){
					$newUserId			= $user->user_id;				
					$channelAssign		= ChannelAssigns::where('user_id', $newUserId)->where('channel_id', $channelId)->first();
					if( !$channelAssign ){
						$channelAssignParam	= [ 'channel_id' => $channelId, 'user_id' => $newUserId, 'created_user_id' => $user_id];
						$channelAssign			= ChannelAssigns::create($channelAssignParam);
					}				
				}else{
					$pwdstr			= str::random(10);
					$password		= Hash::make($pwdstr);
					$userDataParam	= [ 'email' => $request->email, 'first_name' => $request->name, 'password' => $password, 'user_token' => base64_encode(Str::random(10)), 'created_user_id' => $user_id ];
					$user				= User::create($userDataParam);
					$newUserId		= $user->user_id;
					$userData		= ['email' => $request->email];
					$this->forgotpasswordmail($user_data); 
					$channelAssignParam	= [ 'channel_id' => $channelId, 'user_id' => $newUserId, 'created_user_id' => $user_id];
					$channelAssign			= ChannelAssigns::create($channelAssignParam);
				}
				return response()->json(['status' =>'success', 'message'=> 'User Assign to channel successfully', 'channel_assign' => $channelAssign],200);
			}catch(Exception $e){
				return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);
			}				
		}

		public function getUserChannelList( Request $request ){
			$user_id	= $request->auth->user_id;
			try{
				$channelIds	= ChannelAssigns::where('user_id', $user_id)->orWhere('created_user_id', $user_id)->pluck('channel_id')->toArray();
				if( is_array($channelIds) && count($channelIds) > 0 ){
					$channel			= Channel::select('channel_id', 'channel_name')->whereIn('channel_id', $channelIds)->orWhere('user_id', $user_id)->get();
					$channelArray	= $channel->toArray();
					return response()->json(['status' => 'success', 'message' => 'User Channel List Retrieved Succesfully', 'channels' => $channelArray], 200);
				}else{
					$channel			= Channel::select('channel_id', 'channel_name')->where('user_id', $user_id)->get();
					$channelArray	= $channel->toArray();
					if( is_array($channelArray) && count($channelArray) > 0 ){
						return response()->json(['status' => 'success', 'message' => 'User Channel List Retrieved Succesfully', 'channels' => $channelArray], 200);
					}else{
						return response()->json([ 'status' => 'success', 'message' => 'No Channel Created Yet']);
					}
				}
			}catch(Exception $e){
				return response()->json(['status' => 'failed','message' =>  'Application Error - Please see the log for more Information'],520);				
			}

		}

	}
?>