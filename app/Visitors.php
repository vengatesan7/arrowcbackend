<?php
namespace App; 
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;
class Visitors extends Model{
	protected $primaryKey	= 'visitor_id';
	protected $fillable		= ['visitor_client_id', 'client_ip'];   
}
?>