<?php
	namespace App;
	use HasApiTokens, Notifiable;
	use Illuminate\Database\Eloquent\Model;
	class PopupDomainRelations extends Model{
		protected $primaryKey = 'id';
		protected $fillable = ['popup_id', 'domains_id'];
	}
?>
