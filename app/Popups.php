<?php
	namespace App; 
	use HasApiTokens, Notifiable;
	use Illuminate\Database\Eloquent\Model;
	class Popups extends Model{
		protected $primaryKey	= 'popup_id';
		protected $fillable		= ['popup_name', 'popup_img_prvw_url', 'popup_json_code', 'popup_type', 'user_id', 'channel_id', 'archive_status', 'popup_status', 'popup_trigger'];
	}
?>