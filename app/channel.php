<?php namespace App;
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;


class channel extends Model
{
    protected $primaryKey = 'channel_id';
    protected $fillable = ['name'];
}
?>
