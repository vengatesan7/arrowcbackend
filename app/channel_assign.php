<?php namespace App;
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;


class channel_assign extends Model
{
    protected $primaryKey = 'channel_assign_id';
    protected $fillable = ['channel_id', 'campaign_id', 'user_id','created_user_id'];
}
?>
