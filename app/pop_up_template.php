<?php namespace App; 
 use HasApiTokens, Notifiable;
    use Illuminate\Database\Eloquent\Model;
    

    class pop_up_template extends Model
	{
    	protected $primaryKey = 'pop_up_template_id';
        protected $fillable = ['pop_up_template_name','user_id', 'archive_status', 'pop_up_tmpl_img_prvw_url','pop_up_template_json_code','campaign_id', 'trigger'];   
    }
?>