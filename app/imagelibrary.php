<?php namespace App; 
 use HasApiTokens, Notifiable;
    use Illuminate\Database\Eloquent\Model;
    

    class imagelibrary extends Model
	{
    	protected $primaryKey = 'image_id';
        protected $fillable = ['user_id','image_url','image_alt_text'];   
    }
?>