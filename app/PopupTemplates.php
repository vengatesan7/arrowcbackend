<?php
	namespace App; 
	use HasApiTokens, Notifiable;
	use Illuminate\Database\Eloquent\Model;
	class PopupTemplates extends Model{
		protected $primaryKey	= 'popup_template_id';
		protected $fillable		= ['popup_template_name', 'popup_type', 'popup_tmpl_img_prvw_url', 'popup_template_json_code'];
	}
?>