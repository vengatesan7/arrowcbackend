<?php
namespace App;
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;
class Domains extends Model{
	protected $primaryKey = 'domains_id';
	protected $fillable = ['user_id', 'domain_name', 'domain_status'];
}
?>
