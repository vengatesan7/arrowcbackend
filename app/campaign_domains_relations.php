<?php
namespace App;
use HasApiTokens, Notifiable;
use Illuminate\Database\Eloquent\Model;
class campaign_domains_relations extends Model{
	protected $primaryKey = 'id';
	protected $fillable = ['campaign_id', 'domains_id'];
}
?>
