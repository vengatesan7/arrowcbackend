

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Welcome to Bakery</title>
  <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
  
  
  
  
</head>

<body>

  <head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <meta charset="utf-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="description">
  <meta name="author">
  <title>Welcome to Bakery</title>
</head>

<body bgcolor="#fff !important" style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; background-color: #fff !important; margin: 0; padding: 0;"
  class="w-grey">
  <style>
    @media only screen and (max-width: 716px) {

      .small-float-center {

        margin: 0 auto !important;
        float: none !important;
        text-align: center !important;

      }

      .small-text-center {

        text-align: center !important;

      }

      .small-text-left {

        text-align: left !important;

      }

      .small-text-right {

        text-align: right !important;

      }

      .hide-for-large {

        display: block !important;
        width: auto !important;
        overflow: visible !important;
        max-height: none !important;
        font-size: inherit !important;
        line-height: inherit !important;

      }

      table.body table.container .hide-for-large {

        display: table !important;
        width: 100% !important;

      }

      table.body table.container .row.hide-for-large {

        display: table !important;
        width: 100% !important;

      }

      table.body table.container .callout-inner.hide-for-large {

        display: table-cell !important;
        width: 100% !important;

      }

      table.body table.container .show-for-large {

        display: none !important;
        width: 0;
        mso-hide: all;
        overflow: hidden;

      }

      .menu.small-vertical .menu-item {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.body img {

        width: auto;
        height: auto;

      }

      table.body center {

        min-width: 0 !important;

      }

      table.body .container {

        width: 95% !important;

      }

      table.body .columns {

        height: auto !important;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding-left: 16px !important;
        padding-right: 16px !important;

      }

      table.body .column {

        height: auto !important;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding-left: 16px !important;
        padding-right: 16px !important;

      }

      table.body .columns .column {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.body .columns .columns {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.body .column .column {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.body .column .columns {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.body .collapse .columns {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.body .collapse .column {

        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      td.small-1 {

        display: inline-block !important;
        width: 8.33333% !important;

      }

      th.small-1 {

        display: inline-block !important;
        width: 8.33333% !important;

      }

      td.small-2 {

        display: inline-block !important;
        width: 16.66667% !important;

      }

      th.small-2 {

        display: inline-block !important;
        width: 16.66667% !important;

      }

      td.small-3 {

        display: inline-block !important;
        width: 25% !important;

      }

      th.small-3 {

        display: inline-block !important;
        width: 25% !important;

      }

      td.small-4 {

        display: inline-block !important;
        width: 33.33333% !important;

      }

      th.small-4 {

        display: inline-block !important;
        width: 33.33333% !important;

      }

      td.small-5 {

        display: inline-block !important;
        width: 41.66667% !important;

      }

      th.small-5 {

        display: inline-block !important;
        width: 41.66667% !important;

      }

      td.small-6 {

        display: inline-block !important;
        width: 50% !important;

      }

      th.small-6 {

        display: inline-block !important;
        width: 50% !important;

      }

      td.small-7 {

        display: inline-block !important;
        width: 58.33333% !important;

      }

      th.small-7 {

        display: inline-block !important;
        width: 58.33333% !important;

      }

      td.small-8 {

        display: inline-block !important;
        width: 66.66667% !important;

      }

      th.small-8 {

        display: inline-block !important;
        width: 66.66667% !important;

      }

      td.small-9 {

        display: inline-block !important;
        width: 75% !important;

      }

      th.small-9 {

        display: inline-block !important;
        width: 75% !important;

      }

      td.small-10 {

        display: inline-block !important;
        width: 83.33333% !important;

      }

      th.small-10 {

        display: inline-block !important;
        width: 83.33333% !important;

      }

      td.small-11 {

        display: inline-block !important;
        width: 91.66667% !important;

      }

      th.small-11 {

        display: inline-block !important;
        width: 91.66667% !important;

      }

      td.small-12 {

        display: inline-block !important;
        width: 100% !important;

      }

      th.small-12 {

        display: inline-block !important;
        width: 100% !important;

      }

      .columns td.small-12 {

        display: block !important;
        width: 100% !important;

      }

      .column td.small-12 {

        display: block !important;
        width: 100% !important;

      }

      .columns th.small-12 {

        display: block !important;
        width: 100% !important;

      }

      .column th.small-12 {

        display: block !important;
        width: 100% !important;

      }

      table.body td.small-offset-1 {

        margin-left: 8.33333% !important;

      }

      table.body th.small-offset-1 {

        margin-left: 8.33333% !important;

      }

      table.body td.small-offset-2 {

        margin-left: 16.66667% !important;

      }

      table.body th.small-offset-2 {

        margin-left: 16.66667% !important;

      }

      table.body td.small-offset-3 {

        margin-left: 25% !important;

      }

      table.body th.small-offset-3 {

        margin-left: 25% !important;

      }

      table.body td.small-offset-4 {

        margin-left: 33.33333% !important;

      }

      table.body th.small-offset-4 {

        margin-left: 33.33333% !important;

      }

      table.body td.small-offset-5 {

        margin-left: 41.66667% !important;

      }

      table.body th.small-offset-5 {

        margin-left: 41.66667% !important;

      }

      table.body td.small-offset-6 {

        margin-left: 50% !important;

      }

      table.body th.small-offset-6 {

        margin-left: 50% !important;

      }

      table.body td.small-offset-7 {

        margin-left: 58.33333% !important;

      }

      table.body th.small-offset-7 {

        margin-left: 58.33333% !important;

      }

      table.body td.small-offset-8 {

        margin-left: 66.66667% !important;

      }

      table.body th.small-offset-8 {

        margin-left: 66.66667% !important;

      }

      table.body td.small-offset-9 {

        margin-left: 75% !important;

      }

      table.body th.small-offset-9 {

        margin-left: 75% !important;

      }

      table.body td.small-offset-10 {

        margin-left: 83.33333% !important;

      }

      table.body th.small-offset-10 {

        margin-left: 83.33333% !important;

      }

      table.body td.small-offset-11 {

        margin-left: 91.66667% !important;

      }

      table.body th.small-offset-11 {

        margin-left: 91.66667% !important;

      }

      table.body table.columns td.expander {

        display: none !important;

      }

      table.body table.columns th.expander {

        display: none !important;

      }

      table.body .right-text-pad {

        padding-left: 10px !important;

      }

      table.body .text-pad-right {

        padding-left: 10px !important;

      }

      table.body .left-text-pad {

        padding-right: 10px !important;

      }

      table.body .text-pad-left {

        padding-right: 10px !important;

      }

      table.menu {

        width: 100% !important;

      }

      table.menu td {

        width: auto !important;
        display: inline-block !important;

      }

      table.menu th {

        width: auto !important;
        display: inline-block !important;

      }

      table.menu.vertical td {

        display: block !important;

      }

      table.menu.vertical th {

        display: block !important;

      }

      table.menu.small-vertical td {

        display: block !important;

      }

      table.menu.small-vertical th {

        display: block !important;

      }

      table.menu[align="center"] {

        width: auto !important;

      }

      table.button.small-expand {

        width: 100% !important;

      }

      table.button.small-expanded {

        width: 100% !important;

      }

      table.button.small-expand table {

        width: 100%;

      }

      table.button.small-expanded table {

        width: 100%;

      }

      table.button.small-expand table a {

        text-align: center !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.button.small-expanded table a {

        text-align: center !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;

      }

      table.button.small-expand center {

        min-width: 0;

      }

      table.button.small-expanded center {

        min-width: 0;

      }

      .show-for-large {

        display: none !important;
        width: 0;
        mso-hide: all;
        overflow: hidden;

      }

      .hide-for-large {

        display: block !important;
        width: auto !important;
        overflow: visible !important;
        max-height: none !important;
        font-size: inherit !important;
        line-height: inherit !important;

      }

      .margin-auto-small {

        margin: 0 auto;

      }

    }

    @media (min-width: 600px) {

      html {

        font-size: 13px;

      }

    }

    @media (min-width: 800px) {

      html {

        font-size: 14px;

      }

    }

    @media (min-width: 716px) {

      .w-card-block-text-2 {

        padding: 25px 30px 0;

      }

      .w-card:first-of-type {

        padding-right: 30px !important;

      }

      .w-card:nth-of-type(2) {

        padding-left: 30px !important;

      }

    }

    @media only screen and (min-width: 716px) {

      .w-purple .wrapper-inner {

        padding-left: 30px !important;
        padding-right: 30px !important;

      }

    }
  </style>
  <!--[if mso]>

<style type="text/css">

body, table, td {font-family: Arial, Helvetica, sans-serif !important;}

</style>

<![endif]-->

  <table align="center" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; max-width: 600px; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; margin: 0 auto; padding: 0;"
    class="container">
    <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
        <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
          valign="top">
          <!-- 

###################################################



      PREHEADER (view online)

      

#################################################    

    -->
          <table align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; margin: 0; padding: 0;"
            class="wrapper preheader w-no-padding w-no-margin">
            <tbody></tbody>
          </table>
          <!-- 

###################################################



      HEADER 

      

#################################################    

    -->
          <table align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;" class="wrapper header w-outer">
            <tbody>
              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                  class="wrapper-inner" valign="top">
                  <table bgcolor="#fff" align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; background: #fff; margin: 0; padding: 35px 0 0; border 0 transparent #eff0f1;"
                    class="wrapper header w-mine w-inner w-no-padding w-no-margin w-grey">
                    <tbody>
                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                        <td align="right" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: right; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 20px 0 0;"
                          class="wrapper-inner" valign="top">
                          
                          <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; align-items: center; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"
                            class="row">
                            <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
                              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"></tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table bgcolor="#fff" align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; background: #fff; margin: 0; padding: 35px 0 0; border 0 transparent #eff0f1;"
                    class="wrapper header w-inner w-no-padding w-no-margin w-grey">
                    <tbody>
                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                        <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                          class="wrapper-inner" valign="top">
                          <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; align-items: center; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"
                            class="row">
                            <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
                              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                <th align="center" style="width: 600px; text-align: center; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 40px;"
                                  class="w-logo text-center small-12 large-12 columns first last">
                                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                    <tbody>
                                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                        <th align="center" style="color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: center; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                                          class="">
                                          <!--<a href="https://roundclicks.com/"
                                            target="_blank"><span style="display: inline-block;"><img src="https://roundclicks.com/wp-content/themes/innoppl/images/logo.png" alt="Roundclicks" style="width: 50%;outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: 160px; max-width: 100%; clear: both; display: inline-block; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; border: 0;" class="img-logo" width="160"></span></a>--></th>
                                        <th
                                          align="center" style="visibility: hidden; width: 0; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: center; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                                          class="expander">&nbsp;</th>
                                </tr>
                                </tbody>
                                </table>
                                </th>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <!-- 

###################################################



      HERO BLOCK 01 

      

#################################################    

    -->
          <table align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;" class="wrapper hero-block-1 w-outer">
            <tbody>
              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                  class="wrapper-inner" valign="top">
                  <table bgcolor="#fff" align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: center; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; background: #fff; padding: 35px 0 0; border: 1px solid #eff0f1;"
                    class="wrapper hero-block-1 w-inner text-center">
                    <tbody>
                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                        <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 35px 0 36px;"
                          class="wrapper-inner" valign="top">
                          
                          
                          <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; align-items: center; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"
                            class="row">
                            <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
                              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                <th align="left" style="width: 600px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 33px;"
                                  class="w-text-spacing small-12 large-12 columns first last">
                                  
                                  <!-- 

###################################################



      LIST BLOCK (List of elements that have a circular photo left with text right) 

      

#################################################    

    -->
                                
                                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; align-items: center; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding-top: 0;    margin-top: 20px;
                                  "
                                            class="row">
                                            <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
                                              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                                <th align="left" style="width: 600px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 16px 0;"
                                                  class="small-12 large-12 columns first last">
                                                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                                    <tbody>
                                                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                                                        <th align="left" style="color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                                                          class="">

					<p style="font-size:14px;">Resetting your password.</p>
					<p style="font-size:14px;">We received a request to change your Bakery account password.</p><br> 

<div align="center" style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; text-align: center;" class="text-center">
                                                            <!--[if mso]>

                   

                    <![endif]--><a style="border-radius: 3px; color: #ffffff; display: inline-block; font-size: 18px; font-weight: normal; line-height: 48px; text-align: center; text-decoration: none; padding: 0% 5% !important;border-radius: 25px; -webkit-text-size-adjust: none; font-family: 'Heebo', sans-serif !important; position: relative; box-sizing: border-box; background: #7968FF; padding: 0;"
                                                              href="<?php echo $live_url; ?>/reset_password?token=<?php echo $token; ?>"
                                                              class="text-center" target="_blank">Change your password</a>
                                                            <!--[if mso]>

                      </center>

                    </v:roundrect>

                  <![endif]-->
                                                          </div><br>


<!-- <p style="font-size:14px;">You joined thousands of other users just like you!</p>

<p style="font-size:14px;">You're now part of a community of 2,500 users, in teams of all sizes and industries, who chose Leadify to improve their conversions from digital marketing.</p>

<p style="font-size:14px;">Cheers,</p>

<p style="font-size:14px;">The Ba team.</p> -->
                                                          
                                                        </th>
                                                        <th align="left" style="visibility: hidden; width: 0; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                                                          class="expander">&nbsp;</th>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </th>
                                              </tr>
                                            </tbody>
                                          </table>
                                  <!-- 

###################################################



      END LIST BLOCK

      

#################################################    

    -->
                                </th>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>&nbsp;
                  <table align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"
                    class="wrapper footer w-outer">
                    <tbody>
                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                        <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                          class="wrapper-inner" valign="top">
                          <!-- 

###################################################



      FOOTER 

      

#################################################    

    -->
                        </td>
                        <th align="left" style="color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                          class="">
                          <p align="center" style="color: #A1A4AA; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: center; line-height: 16px; font-size: 11px; position: relative; box-sizing: border-box; margin: 0 0 20px; padding: 0;"
                            class="text-footer spacing-large">You've received this email as confirmation of your Leadify account.</p>
                        </th>
                        <th align="left" style="visibility: hidden; width: 0; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0;"
                          class="expander">&nbsp;</th>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          
        </td>
      </tr>
    </tbody>
  </table>
  <table bgcolor="#fff" align="center" style="width: 100%; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; background: #fff; padding: 35px 0 0; border 0 transparent #eff0f1;"
    class="wrapper footer w-inner w-grey">
    <tbody>
      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
        <td align="left" style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0; padding: 0 0 36px;"
          class="wrapper-inner" valign="top">
          <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 80% !important; position: relative; display: table; align-items: center; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; margin: 0 auto; padding: 0;"
            class="row w-80">
            <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                <th align="left" style="width: 600px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 16px 16px;"
                  class="small-12 large-12 columns first last">
                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                    <tbody></tbody>
                  </table>
                </th>
              </tr>
            </tbody>
          </table>
          <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: table; align-items: center; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"
            class="row">
            <tbody style="position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important;">
              <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                <th align="left" style="width: 217.33333px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 8px 16px 16px;"
                  class="small-4 large-4 columns first">
                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                    <tbody></tbody>
                  </table>
                </th>
                <th align="left" style="width: 100.66667px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 8px 16px;"
                  class="center-img small-2 large-2 columns">
                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                    <tbody>
                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"></tr>
                    </tbody>
                  </table>
                </th>
                <th align="left" style="width: 100.66667px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 8px 16px;"
                  class="center-img small-2 large-2 columns">
                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                    <tbody>
                      <tr align="left" style="vertical-align: top; text-align: left; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;"></tr>
                    </tbody>
                  </table>
                </th>
                <th align="left" style="width: 217.33333px; color: #0a0a0a; font-family: 'Heebo', sans-serif !important; font-weight: normal; text-align: left; line-height: 130%; font-size: 16px; position: relative; box-sizing: border-box; margin: 0 auto; padding: 0 16px 16px 8px;"
                  class="small-4 large-4 columns last">
                  <table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; box-sizing: border-box; font-family: 'Heebo', sans-serif !important; padding: 0;">
                    <tbody></tbody>
                  </table>
                </th>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>
  
  

</body>

</html>
