<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Popup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       

        Schema::create('pop_up_masters', function (Blueprint $table) {
        $table->bigIncrements('pop_up_id');
        $table->integer('campaign_type_id');
        $table->string('pop_up_name',100);
        $table->text('pop_up_img_prvw_url');
        $table->text('pop_up_json_code');
        $table->integer('user_id')->nullable();
        $table->boolean('archive_status')->default(false);
        $table->timestamps();
        });        

        Schema::create('pop_up_templates', function (Blueprint $table) {
        $table->bigIncrements('pop_up_template_id');
        $table->string('pop_up_template_name',100);
        $table->integer('campaign_id');
        $table->integer('user_id');
        $table->boolean('archive_status')->default(false);
        $table->string('pop_up_tmpl_img_prvw_url',400);
        $table->text('pop_up_template_json_code');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
