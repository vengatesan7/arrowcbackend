<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_elements', function (Blueprint $table) {
            $table->bigIncrements('element_id');
            $table->bigInteger('contact_id')->nullable();
            $table->string('element_name', 400)->nullable();
            $table->text('element_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_elements');
    }
}
