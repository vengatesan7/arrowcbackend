<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Channel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('channel_id');
            $table->integer('name');
            $table->integer('created_user_id');
            $table->timestamps();
        });

        Schema::create('channel_assigns', function (Blueprint $table) {
            $table->increments('channel_assign_id');
            $table->integer('channel_id');
            $table->integer('campaign_id');
            $table->integer('user_id');
            $table->integer('created_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
