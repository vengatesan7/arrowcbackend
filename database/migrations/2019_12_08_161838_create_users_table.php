<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('email',40)->unique();
            $table->string('password',200)->nullable();
            $table->string('first_name',40)->nullable();
            $table->string('last_name',40)->nullable();
            $table->string('mobile_number',20)->nullable();
            $table->string('ip_address',20)->nullable();
            $table->unsignedInteger('current_plan_id')->nullable();
            $table->time('plan_activation_date')->nullable();
            $table->string('user_token',40)->nullable();
            $table->boolean('status')->nullable();
            $table->integer('role_id');
            $table->integer('created_user_id')->nullable();
            $table->timestamps();
        });

        Schema::create('role_masters', function (Blueprint $table) {
            $table->increments('role_id');
            $table->string('name',50)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
