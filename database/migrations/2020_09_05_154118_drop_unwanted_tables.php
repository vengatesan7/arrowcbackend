<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUnwantedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //        
        Schema::dropIfExists('leads');
        Schema::dropIfExists('lead_elements');
        Schema::dropIfExists('channel_assigns');
        Schema::dropIfExists('channels');
        Schema::dropIfExists('pop_up_masters');
        Schema::dropIfExists('pop_up_templates');
        Schema::dropIfExists('campaign_domains_relations');
        Schema::dropIfExists('campaigns');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
