<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popup_templates', function (Blueprint $table) {
            $table->bigIncrements('popup_template_id');
            $table->string('popup_template_name',100)->nullable();
            $table->boolean('popup_status')->default(true);
            $table->string('popup_type', 400)->nullable();
            $table->string('popup_tmpl_img_prvw_url',400)->nullable();
            $table->text('popup_template_json_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popup_templates');
    }
}
