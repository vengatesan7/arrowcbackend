<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popups', function (Blueprint $table) {
            $table->bigIncrements('popup_id');
            $table->string('popup_name',100);
            $table->text('popup_img_prvw_url');
            $table->text('popup_json_code');
            $table->string('popup_type', 400)->nullable();
            $table->integer('user_id')->nullable();
            $table->boolean('archive_status')->default(false);
            $table->boolean('popup_status')->default(true);
            $table->text('popup_trigger')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popups');
    }
}
