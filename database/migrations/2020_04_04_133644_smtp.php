<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Smtp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smtp_mail_configs', function (Blueprint $table) {
            $table->increments('smtp_config_id');
            $table->string('smtp_from_email',100);
            $table->string('smtp_from_name',100);
            $table->string('smtp_from_username',100);
            $table->string('smtp_from_password',100);
            $table->string('smtp_from_host',100);
            $table->string('smtp_from_port',100);
            $table->string('smtp_type',50);
            $table->integer('created_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
