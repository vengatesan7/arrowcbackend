<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Leads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('lead_id');
            $table->integer('campaign_id');
            $table->integer('created_user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('lead_elements', function (Blueprint $table) {
            $table->increments('element_id');
            $table->integer('lead_id');
            $table->text('element_name');
            $table->text('element_value');
            $table->integer('created_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
