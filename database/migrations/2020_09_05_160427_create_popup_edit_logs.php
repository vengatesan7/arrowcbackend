<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupEditLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popup_edit_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('popup_id')->nullable();            
            $table->bigInteger('user_id')->nullable();
            $table->text('old_popup_json_code');
            $table->text('updated_popup_json_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popup_edit_logs');
    }
}
